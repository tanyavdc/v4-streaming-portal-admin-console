var replaceall = require("replaceall");

exports.formatFormStringsForSQL = function(req, res, next) {

  // ASYNC? ? **
  var data = req.body;

  if (Object.keys(data).length > 0){
    for (var field in data){
     
      // replace single quotes with two single quotes to not break sql
      data[field]= replaceall("'","''",data[field]);
     
      }
  }
  return next(); 
};



