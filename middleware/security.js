exports.ensureAuthenticated = function(req, res, next) {

    var sess = req.session;
	  if (req.isAuthenticated()){
		    return next();
		  }
		  else{
        
        sess.redirect_to = req.path;
        console.log("the redirect path " + req.path); 
        req.flash('loginMessage', 'You must be logged in to access this content');
        res.redirect('/login')
		  }
		};


// INCOMPLETE ** 
exports.ensureEnabled = function(type) {

  return function(req,res,next){
    console.log("the type " + type);
    var tag = type + "_enabled"; 
    if (req.app.locals[tag] == true){
     return next(); 
   }
    else{
      // redirect with flash msg *** 
      res.render('home',{msg:'Page is not available'});

    }
  }
};


