// setup app local variables by fetching site specific values from redis cache


var cache= require('../model/cache.js');

var sess;

module.exports = function(req, res, next) {

	sess = req.session;

  if(!sess.user){
    sess.user = {logged_in:false}; 
  }


	// get categories

 var livestream_categories_tag =  "livestream-categories-" + sess.current_language;
  cache.lrange(livestream_categories_tag,0,-1, function(err,reply) {

				 req.app.locals.livestream_categories = listToMap(reply);

			 });

	// get ondemand categories

	var ondemand_categories_tag = "ondemand-categories-" + sess.current_language;
			 cache.lrange(ondemand_categories_tag,0,-1, function(err,reply) {
				 req.app.locals.ondemand_categories = listToMap(reply);

			 });


	var advertisement_categories_tag = "advertisement-categories-" + sess.current_language;
			 cache.lrange(advertisement_categories_tag,0,-1, function(err,reply) {
				 req.app.locals.advertisement_categories = listToMap(reply);
			 });


	cache.lrange('enabled-languages',0,-1,function(err,reply){
		req.app.locals.enabled_languages = reply;
	});

 // get admin-set domain settings

  cache.hgetall('domain',function(err,reply){

    req.app.locals.settings = reply;


    // if not current language set, set to default
    if (!sess.current_language){
	    	sess.current_language = config.default_language
	    }

    cache.lrange("enabled-services",0,-1, function(err,reply) {


  		req.app.locals.settings['enabled-services'] = reply;

      var account_type = req.app.locals.settings['payment-gateway'];
        next();
     });
  });
};

// ** HELPERS **

// listToMap([2:a,3:b,somekey:somevalue]) => {2:a,3:b,somekey:somevalue}
function listToMap(list){
  var map = {};
  for (var i = 0 ; i < list.length ; i ++){
    var pair = list[i];
    var key = pair.split(":")[0];
    var value = pair.split(":")[1];
    map[key]=value;

  }
 return map;

}
