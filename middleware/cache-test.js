var cache = require('../model/cache.js');
require('jquery');


exports.getCurrentLang = function(callback){

   cache.hexists('domain','default-language',function(err, reply){
   // default language not set. set to english 
   if (reply === 0 ){
     cache.hset('domain','default-language','en');
     // save to app variable
     return callback('en');
   }
   
     cache.hget('domain','default-language',function(err,reply){
       console.log("default lang reply " + reply);
      return callback(reply);

     });
   });
  
};
