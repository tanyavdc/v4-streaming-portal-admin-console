var express = require('express'),
  router = express.Router();
var model = require('../model/streams.js');
var cache = require('../model/cache');
var passport = require('passport');
var flash = require('connect-flash');
var security = require('../middleware/security.js');
var multer = require('multer');
var path = require('path');
var sess;

var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './public/tmp/thumbs/')
  },
  filename: function(req, file, cb) {
    cb(null, file.fieldname + Date.now())
  }
})

var video_storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './public/tmp/videos/')
  },
  filename: function(req, file, cb) {
    cb(null, file.fieldname + Date.now() + path.extname(file.originalname))
  }
})

var upload_thumb = multer({
  storage: storage
});

var upload_video = multer({
  storage: video_storage
});


//  GETS

router.get('/edit_ondemand/:id', security.ensureAuthenticated, function(req, res) {

  sess = req.session;

  var streams_by_language;

  var current_language = sess.current_language;

  var category_label_id_map = req.app.locals.ondemand_categories
    // list the available categories in this type of stream to pass to
    // tokenfield autocomplete in frontend
  var available_categories = Object.keys(category_label_id_map).
  map(function(key) {
    return category_label_id_map[key]
  })

  var stream_id;
  var got_stream = false;

  var default_language = config.default_language;
  model.getStreamByID(req.params.id, 'ondemand', function(err, data) {

    stream_id = data.id;

    model.getCategoryLabelsForStream(stream_id, current_language,
      function(categories) {

        model.getAllStreamTranslations(stream_id,
          function(err, titles, descriptions) {
            // get ads tagged to this stream
            getAds(stream_id, current_language, function(advertisements) {

              model.getAdTitles(current_language, function(err, ads) {

                model.getTagsForStream(stream_id, function(tags) {

                  res.render('edit-ondemand', {
                    title: req.params.type,
                    available_categories: available_categories,
                    stream: data,
                    stream_categories: categories,
                    stream_advertisements: advertisements,
                    advertisements: ads,
                    titles: titles,
                    descriptions: descriptions,
                    id: req.params.id,
                    tags: tags,
                    session: sess
                  });
                });
              });
            });
          });
      });
  });
})

router.get('/edit_livestream/:id', security.ensureAuthenticated, function(req, res) {

  var streams_by_language;
  sess = req.session;

  var current_language = sess.current_language;

  var stream_id;
  var got_stream = false;

  var default_language = config.default_language;

  var category_label_id_map = req.app.locals.livestream_categories
    // list the available categories in this type of stream to pass to
    // tokenfield autocomplete in frontend
  var available_categories = Object.keys(category_label_id_map).
  map(function(key) {
    return category_label_id_map[key]
  })

  model.getStreamByID(req.params.id, 'livestream', function(err, data) {

    stream_id = data.id;

    model.getCategoryLabelsForStream(stream_id, current_language, function(categories) {

      model.getAllStreamTranslations(stream_id, function(err, titles, descriptions) {

        // get ads tagged to this stream
        getAds(stream_id, current_language, function(advertisements) {

          // get all available ads
          model.getAdTitles(current_language, function(err, ads) {

            model.getTagsForStream(stream_id, function(tags) {

              res.render('edit-livestream', {
                title: req.params.type,
                available_categories: available_categories,
                stream: data,
                stream_categories: categories,
                stream_advertisements: advertisements,
                advertisements: ads,
                titles: titles,
                descriptions: descriptions,
                id: req.params.id,
                tags: tags,
                session: sess
              });
            });
          });
        });
      });
    });
  });
})


router.get('/edit_advertisement/:id', security.ensureAuthenticated, function(req, res) {

  var streams_by_language;
  sess = req.session;
  var stream_id;
  var current_language = sess.current_language;
  var default_language = config.default_language;

  var category_label_id_map = req.app.locals.advertisement_categories
    // list the available categories in this type of stream to pass to
    // tokenfield autocomplete in frontend
  var available_categories = Object.keys(category_label_id_map).
  map(function(key) {
    return category_label_id_map[key]
  })

  model.getStreamByID(req.params.id, 'advertisement', function(err, data) {

    console.log("in main route got data " + JSON.stringify(data));
    stream_id = data.id;

    model.getCategoryLabelsForStream(stream_id, current_language, function(categories) {

      model.getAllStreamTranslations(stream_id, function(err, titles,
        descriptions) {

        model.getTagsForStream(stream_id, function(tags) {
          console.log("the tags " + tags)

          res.render('edit-advertisement', {
            title: req.params.type,
            available_categories: available_categories,
            stream: data,
            titles: titles,
            descriptions: descriptions,
            stream_categories: categories,
            id: req.params.id,
            tags: tags,
            session: sess
          });

        });
      });
    });
  });
});


// get streams by type

router.get('/stream_by_type/:type', function(req, res) {

  sess = req.session;
  var type = req.params.type;
  var type_categories = req.app.locals[type + "_" + "categories"]

  model.getAllStreamsOfType(req.params.type, sess.current_language, function(err, results) {

    // if no stream entries for this language
    if (results == null || Object.keys(results).length == 0) {

      // get stream info in default LANGUAGE
      model.getAllStreamsOfType(req.params.type, config.default_language, function(err, default_results) {

        model.getStreamCountsByType(req.params.type, config.default_language, function(err, counts) {
          res.render('streams-layout', {
            title: req.params.type,
            type: req.params.type,
            category: {
              id: null,
              label: 'all'
            },
            categories: type_categories,
            streams: default_results,
            counts: counts,
            session: sess
          });
        });
      });

    } else {

      model.getStreamCountsByType(req.params.type, sess.current_language, function(err, counts) {

        res.render('streams-layout', {
          title: req.params.type,
          type: req.params.type,
          category: {
            id: null,
            label: 'all'
          },
          categories: type_categories,
          streams: results,
          session: sess,
          counts: counts
        });
      });

    };
  });
});

// get streams by category
router.get('/stream_by_category/:type/:id', function(req, res) {

  var type = req.params.type;
  var category = req.params.id;
  var default_language = config.default_language;
  sess = req.session;

  // get categories map for stream by type
  var type_categories = req.app.locals[type + "_" + "categories"]
  var category_label = type_categories[category];

  model.getTypeStreamsOfCategory(req.params.type, sess.current_language,
    category,
    function(err, results) {

      // if no stream entries for this language
      if (results == null || Object.keys(results).length == 0) {

        // get stream info in default LANGUAGE
        model.getTypeStreamsOfCategory(req.params.type,
          default_language, category,
          function(err, default_results) {

            model.getStreamCountsByType(req.params.type,
              config.default_language,
              function(err, counts) {

                res.render('streams-layout', {
                  title: req.params.type,
                  type: req.params.type,
                  category: {
                    id: category,
                    label: category_label
                  },
                  categories: type_categories,
                  streams: default_results,
                  session: sess,
                  counts: counts
                });

              })
          });
      }
      // got results in current language
      else {

        model.getStreamCountsByType(req.params.type, sess.current_language, function(err, counts) {

          res.render('streams-layout', {
            title: req.params.type,
            type: req.params.type,
            category: {
              id: category,
              label: category_label
            },
            categories: type_categories,
            streams: results,
            counts: counts,
            session: sess
          });
        });
      }
    });
});

router.get('/', function(req, res) {
  // to home page where recently watched and favourites are displayed IF SIGNED ON AS CUSTOMER **
  sess = req.session;

  console.log("getting HOME");
  res.render('home', {
    title: 'Home',
    session: sess
  });

});

router.get('/goservices', security.ensureEnabled('goservices'), function(req, res) {
  sess = req.session;
  model.getGoServices(function(err, results) {

    console.log("results from route " + JSON.stringify(results));
    res.render('goservices', {
      title: 'goservices',
      goservices: results,
      session: sess
    })

  })

})

router.get('/add_goservice', security.ensureAuthenticated, function(req, res) {
  sess = req.session;

  res.render('add-goservice', {
    title: 'goservice',
    session: sess
  });
})

router.get('/edit_goservice', security.ensureAuthenticated, function(req, res) {
  sess = req.session;

  res.render('edit-goservice', {
    title: 'goservice',
    session: sess
  });
})

// get add livestream form

router.get('/add_livestream/:category', security.ensureAuthenticated, function(req, res) {
  sess = req.session;
  var category_id = req.params.category;
  var category_label_id_map = req.app.locals.livestream_categories

  var category_label = category_label_id_map[category_id];
  // list the available categories in this type of stream to pass to
  // tokenfield autocomplete in frontend
  var available_categories = Object.keys(category_label_id_map).
  map(function(key) {
    return category_label_id_map[key]
  })

  console.log("the categories " + available_categories);

  var current_language = sess.current_language;

  // get category id from front end

  // call to model to get ads **
  model.getAdTitles(current_language, function(err, ads) {

    console.log("the ads " + JSON.stringify(ads));

    res.render('add-livestream', {
      title: 'livestream',
      category: {
        id: category_id,
        label: category_label
      },
      advertisements: ads,
      session: sess,
      available_categories: available_categories
    });
  })
});


// get add ondemand form
router.get('/add_vod/:category', security.ensureAuthenticated,
  function(req, res) {

    sess = req.session;
    var current_language = sess.current_language;
    var category_id = req.params.category;
    var category_label_id_map = req.app.locals.ondemand_categories

    var category_label = category_label_id_map[category_id];
    // list the available categories in this type of stream to pass to
    // tokenfield autocomplete in frontend
    var available_categories = Object.keys(category_label_id_map).
    map(function(key) {
      return category_label_id_map[key]
    })

    console.log("the categories " + available_categories);

    model.getAdTitles(current_language, function(err, ads) {

      console.log("the ads vod " + JSON.stringify(ads));

      // to model to get favourites for this customer id

      res.render('add-ondemand', {
        title: 'add vod',
        type: 'ondemand',
        category: {
          id: category_id,
          label: category_label
        },
        available_categories: available_categories,
        advertisements: ads,
        session: sess
      });
    });

});


router.get('/add_advertisement/:category', security.ensureAuthenticated,
  function(req, res) {

    sess = req.session;


    var category_id = req.params.category;
    var category_label_id_map = req.app.locals.advertisement_categories

    var category_label = category_label_id_map[category_id];
    // list the available categories in this type of stream to pass to
    // tokenfield autocomplete in frontend
    var available_categories = Object.keys(category_label_id_map).
    map(function(key) {
      return category_label_id_map[key]
    })

    console.log("the categories " + available_categories);

    res.render('add-advertisement', {
      title: 'test',
      category: {
        id: category_id,
        label: category_label
      },
      available_categories: available_categories,
      session: sess
    });

});


// POSTS

// add livestream

router.post('/add_livestream', [security.ensureAuthenticated,
  sanitizer.formatFormStringsForSQL
], function(req, res) {
  // to model to get livestreams of category

  sess = req.session;
  var ads = req.body.advertisements
  var tags = req.body.tags.trim().split(',');
  var category_labels = req.body.categories.trim().split(',');
  var categories_id_map = req.app.locals.livestream_categories;
  var categories = listKeysForValues(categories_id_map, category_labels);

  getAdIds(ads, function(ad_ids) {

    console.log("the ad ids " + ad_ids);

    // passing true to indicate that this stream is new
    model.addLiveStream(req, ad_ids, function(err, stream_id) {
      console.log("add livestream id " + stream_id);
      model.tagStreamToCategories(stream_id, categories,
        function(err) {

          // TAG STREAM TO TAGS
          model.tagStreamToTags(tags, stream_id, function(err) {

            console.log("err in addlivestream " + err)

            res.render('Success', {
              title: 'Success',
              msg: 'Successful new livestream add',
              btn_name: 'Back to livestreams',
              action: '/stream_by_type/livestream',
              session: sess
            });

          });
        });
    });
  });
});


// ENSURE AUTHENTICATED **
router.post('/add_vod', [security.ensureAuthenticated,
  sanitizer.formatFormStringsForSQL
], function(req, res) {

  sess = req.session;
  var ads = req.body.advertisements
  var tags = req.body.tags.trim().split(',');

  var category_labels = req.body.categories.trim().split(',');
  var categories_id_map = req.app.locals.ondemand_categories;
  var categories = listKeysForValues(categories_id_map, category_labels);

  getAdIds(ads, function(ad_ids) {

    console.log("the ad ids " + ad_ids);


    model.addVOD(req, ad_ids, function(err, stream_id) {
      console.log("added vod");

      model.tagStreamToCategories(stream_id, categories, function(err) {

        model.tagStreamToTags(tags, stream_id, function(err) {

          res.render('Success', {
            title: 'Success',
            msg: 'Successful new VOD add',
            btn_name: 'Back to VOD content',
            action: '/stream_by_type/ondemand',
            session: sess
          });
        });
      });
    });
  });
});


router.post('/add_advertisement', [security.ensureAuthenticated,
  sanitizer.formatFormStringsForSQL
], function(req, res) {

  sess = req.session;
  var tags = req.body.tags.trim().split(',');

  var category_labels = req.body.categories.trim().split(',');
  var categories_id_map = req.app.locals.advertisement_categories;
  var categories = listKeysForValues(categories_id_map, category_labels);


  model.addAdvertisement(req, function(err, stream_id) {
    model.tagStreamToCategories(stream_id, categories, function(err) {

      model.tagStreamToTags(tags, stream_id, function(err) {

        res.render('Success', {
          title: 'Success',
          msg: 'Successful new Advertisement add',
          btn_name: 'Back to Ad content',
          action: '/stream_by_type/advertisement',
          session: sess
        });
      });
    });
  });
});



// add categories
router.post('/add_categories/:type', [security.ensureAuthenticated,
  sanitizer.formatFormStringsForSQL
], function(req, res) {
  sess = req.session;
  console.log("req body " + JSON.stringify(req.body));
  var count = req.body.count;
  var type = req.params.type;

  var raw_langs = req.body.languages;
  var available_languages = raw_langs.split(',');
  console.log("the avail langs " + available_languages);

  // group json msg
  groupTranslationsByCategory(req.body, function(category_translations_map) {

    console.log("the results in route from helper " +
      JSON.stringify(category_translations_map))

    addCategories(category_translations_map, type, available_languages,
      function(err, categories_map) {

        if (!err) {

          categoriesToCache(req, categories_map, type, function(err) {

            var btn_name = 'Back to ' + req.params.type + ' content';
            var action = '/stream_by_type/' + req.params.type;
            var msg = 'Successful new ' + req.params.type + ' category add';

            res.render('Success', {
              title: 'Success',
              msg: msg,
              btn_name: btn_name,
              action: action,
              session: sess
            });

          })
        } else {
          console.log("an error in route " + err);
          res.status(500)
          res.render('error', {
            error: err,
            session: sesss
          })
        }

      })
  });
});



router.post('/upload_livethumb', upload_thumb.single('livestreamThumb'), function(req, res) {

  var tmp_path = req.file.path;
  return res.status(200).send(req.file);

});

router.post('/upload_vodthumb', upload_thumb.single('vodThumb'), function(req, res) {

  var tmp_path = req.file.path;
  return res.status(200).send(req.file);

});

router.post('/upload_gothumb', upload_thumb.single('goserviceThumb'), function(req, res) {

  var tmp_path = req.file.path;
  return res.status(200).send(req.file);

});


router.post('/upload_adthumb', upload_thumb.single('adThumb'), function(req, res) {

  var tmp_path = req.file.path;
  return res.status(200).send(req.file);

});

router.post('/upload_vod', upload_video.single('vodVid'), function(req, res) {

  var tmp_path = req.file.path;
  return res.status(200).send(req.file);

});

router.post('/upload_ad', upload_video.single('adVid'), function(req, res) {

  var tmp_path = req.file.path;
  return res.status(200).send(req.file);

});

router.post('/add_goservice', security.ensureAuthenticated, function(req, res) {
  model.addGoService(req, function(err) {
    res.redirect('back');
  });
})

router.post('/remove_goservice', security.ensureAuthenticated, function(req, res) {
  var goservice_id = req.body.id;

  model.removeGoService(goservice_id, function(err) {
    res.redirect('back');
  });
})

router.post('/remove_stream/:id', security.ensureAuthenticated, function(req, res) {
  var stream_id = req.params.id;
  var type = req.body.type;

  model.removeStream(stream_id, type, function(err) {
    res.redirect('back');
  });

});

router.post('/remove_category/:type/:id', security.ensureAuthenticated, function(req, res) {

  var category = req.params.id;
  var type = req.params.type;
  var enabled_languages = req.app.locals.enabled_languages;

  model.removeCategory(category, function(err) {

    // remove category from cache
    removeCategoryTranslationsFromCache(category, type, enabled_languages, function() {

      res.send("done");

    })
  });
});

router.post('/edit_livestream/:id', security.ensureAuthenticated, function(req, res) {

  sess = req.session;
  var stream_id = req.params.id;
  var tags = req.body.tags.trim().split(',');
  var current_language = sess.current_language;
  var ads = req.body.advertisements;

  var category_labels = req.body.categories.trim().split(',');
  var categories_id_map = req.app.locals.livestream_categories;
  var categories = listKeysForValues(categories_id_map, category_labels);

  model.editLiveStream(stream_id, req, function(err) {
    if (err) {
      // stream DNE
      res.render('error', {
        title: 'Error',
        msg: 'Stream with id ' + stream_id + " no longer exists",
        btn_name: 'Back to Livestream content',
        action: '/stream_by_type/livestream',
        session: sess
      });
    }
    model.clearCategories(stream_id, function() {
      model.clearTags(stream_id, function() {
        // add new categories and tags
        model.tagStreamToCategories(stream_id, categories, function(err) {
          model.tagStreamToTags(tags, stream_id, function(err) {
            model.clearAds(stream_id, function() {
              getAdIds(ads, function(ad_ids) {
                model.tagStreamToAds(stream_id, ad_ids, function(err) {
                  // stream DNE
                  if (err) {
                    res.render('error', {
                      title: 'Error',
                      msg: 'Stream with id ' + stream_id + " no longer exists",
                      btn_name: 'Back to Livestream content',
                      action: '/stream_by_type/livestream',
                      session: sess
                    });
                  } else {
                    res.render('Success', {
                      title: 'Success',
                      msg: 'Successful livestream edit',
                      btn_name: 'Back to livestream content',
                      action: '/stream_by_type/livestream',
                      session: sess
                    });
                  }
                })
              })
            })
          })
        })
      })
    })
  })
});


router.post('/edit_advertisement/:id', security.ensureAuthenticated, function(req, res) {
  sess = req.session;
  var stream_id = req.params.id;
  var tags = req.body.tags.trim().split(',');
  var current_language = sess.current_language;

  var category_labels = req.body.categories.trim().split(',');
  var categories_id_map = req.app.locals.advertisement_categories;
  var categories = listKeysForValues(categories_id_map, category_labels);

  model.editAdvertisement(stream_id, req, function(err) {
    if (err) {
      // stream DNE
      res.render('error', {
        title: 'Error',
        msg: 'Stream with id ' + stream_id + " no longer exists",
        btn_name: 'Back to Ad content',
        action: '/stream_by_type/advertisement',
        session: sess
      });
    }
    model.clearCategories(stream_id, function() {
      model.clearTags(stream_id, function() {
        // add new categories and tags
        model.tagStreamToCategories(stream_id, categories, function(err) {
          model.tagStreamToTags(tags, stream_id, function(err) {
            if (!err) {
              res.render('Success', {
                title: 'Success',
                msg: 'Successful Advertisement edit',
                btn_name: 'Back to Ad content',
                action: '/stream_by_type/advertisement',
                session: sess
              });
            } else {
              res.status(500);
              res.render('error', {
                title: 'Error',
                msg: 'Stream with id ' + stream_id + " no longer exists",
                btn_name: 'Back to Ad content',
                action: '/stream_by_type/adverisement',
                session: sess
              });
              return;

            }
          })
        })
      })
    })
  })
});

router.post('/edit_ondemand/:id', security.ensureAuthenticated, function(req, res) {

  sess = req.session;
  var stream_id = req.params.id;
  var tags = req.body.tags.trim().split(',');
  var current_language = sess.current_language;
  var ads = req.body.advertisements

  var category_labels = req.body.categories.trim().split(',');
  var categories_id_map = req.app.locals.ondemand_categories;
  var categories = listKeysForValues(categories_id_map, category_labels);

  // catch any error in end  *** TODO
  // scenario : stream can be deleted anywhere within here
  model.editVOD(stream_id, req, function(err) {
    // stream DNE
    if (err) {
      console.log("Stream DNE");
      res.status(500);
      res.render('error', {
        title: 'Error',
        msg: 'Stream with id ' + stream_id + " no longer exists",
        btn_name: 'Back to VOD content',
        action: '/stream_by_type/ondemand',
        session: sess
      });
      return;
    }
    model.clearCategories(stream_id, function() {
      model.clearTags(stream_id, function() {
        // add new categories and tags
        model.tagStreamToCategories(stream_id, categories, function(err) {
          model.tagStreamToTags(tags, stream_id, function(err) {
            model.clearAds(stream_id, function() {
              getAdIds(ads, function(ad_ids) {
                console.log("the ad ids " + ad_ids);
                model.tagStreamToAds(stream_id, ad_ids, function(err) {
                  if (!err) {
                    res.render('Success', {
                      title: 'Success',
                      msg: 'Successful VOD edit',
                      btn_name: 'Back to VOD content',
                      action: '/stream_by_type/ondemand',
                      session: sess
                    });
                  } else {
                    res.status(500);
                    res.render('home', {
                      title: 'error',
                      msg: 'Stream with id ' + stream_id + " no longer exists",
                      btn_name: 'Back to VOD content',
                      action: '/stream_by_type/ondemand',
                      session: sess
                    });
                    return;

                  }
                })
              })
            })
          })
        })
      })
    })
  })
});

router.post('/edit_category/:category_id', security.ensureAuthenticated, function(req, res) {
  sess = req.session;

  var category_id = req.params.category_id
  var new_label = req.body['new-label'];
  var language = req.body['language'];
  var type = req.body['type'];

  model.editCategory(category_id, new_label, language, type, function(err) {

    changeCachedCategory(type, category_id, new_label, language, function(err) {

      res.send("done");
    });
  });
});


// **** HELPERS ******

function getAds(stream_id, current_lang, callback) {

  var ads = [];

  model.getStreamAds(stream_id, current_lang, function(err, results) {
    // formatted as id:label
    listAdvertisements(results, function(ads) {

      callback(ads);
    })

  });
}


function listAdvertisements(streams, callback) {
  var ads = [];

  for (var i = 0; i < streams.length; i++) {
    var ad_label = streams[i].ad_id + ": " + streams[i].ad_title;
    ads.push(ad_label);
  }
  if (i == streams.length) {
    if (typeof callback == 'function') {

      callback(ads[0]);
    }
  }

}


// format html form body data
function groupTranslationsByCategory(form_data, callback) {

  // {1 : label-en,label-fr , 2: etc }
  var formated_data = {}


  var count = form_data.count;
  var raw_langs = form_data.languages;
  var available_languages = raw_langs.split(',');

  console.log("count " + count);
  console.log("availabled " + available_languages);

  // for category in new categories list
  for (var i = 0; i < count; i++) {
    // translations for this category
    var translations = {};
    // get the translated labels
    for (var j = 0; j < available_languages.length; j++) {
      // cateogry number i
      // language j

      // get [index]-label-[lang]
      var lang = available_languages[j];
      var index = i + 1;
      var translated_label_tag = index + "-label-" + lang

      var translated_label = form_data[translated_label_tag];
      translations[lang] = translated_label;



    }
    formated_data[i] = translations;
  }

  console.log(JSON.stringify(formated_data));
  if (i == count) {
    if (typeof callback == 'function') {

      callback(formated_data);
    }
  }

}

// {"0":{"en":"heaven1","fr":"heavenfr"},
// "1":{"en":"heaven2","fr":"heaven2fr"}}
function addCategories(categories, type, langs, callback) {

  //{"0":{"en":"test en","fr":"test fr"},"1":{"en":"test 2 en","fr":"test 2 fr”}}
  var count = Object.keys(categories).length

  // map of added category id to a list of its label translations
  // {id:{en:label,french:label}}
  var added_categories = {}

  var added_count = 0;

  for (var category in categories) {
    // 0 : {en:label,fr:label}

    var category_translations = categories[category];
    console.log("the category translatiosn " +
      JSON.stringify(category_translations));
    model.addCategory(category_translations, type, langs,
      function(err, id, labels) {

        console.log("the new cat id " + id);

        if (!err) {

          added_categories[id] = labels;
          added_count++;
          console.log("added " + added_count);

          if (added_count >= count) {
            console.log("done adding categories")
            console.log("in addCategories the map " +
              JSON.stringify(added_categories))
            callback(undefined, added_categories);
          }

        } else {
          callback(err, undefined);
        }

      });
  }
}


function getAdIds(ads, callback) {

  var raw_ads = ads.trim().split(',');
  console.log("the raw ads from helper " + raw_ads)
  var ids = [];

  for (var i = 0; i < raw_ads.length; i++) {
    var id = raw_ads[i].split(":")[0];

    if (id.trim() != "") {
      console.log("THE ID " + id);
      ids.push(id);
    }
  }

  if (i == raw_ads.length) {
    if (typeof callback == 'function') {
      console.log("the ids from helper " + ids);
      callback(ids);
    }
  }

}

function clone(a) {
  return JSON.parse(JSON.stringify(a));
}



// {id:{en:label,french:label},id:{en:label,french:label}}
// ** REVIEW
function categoriesToCache(req, categories_map, type, callback) {

  console.log("in categories to cache the map " +
    JSON.stringify(categories_map))


  for (var id in categories_map) {
    // id : {en:label,french:label}
    console.log("id " + JSON.stringify(categories_map[id]));
    var translations = categories_map[id];
    // en:label,french:label
    for (var lang in translations) {
      console.log("lang " + JSON.stringify(lang));
      // en:label
      var id2translation = id + ":" + translations[lang];
      console.log("the id2translation " + id2translation)

      var cat_tag = type + '-categories-' + lang;
      console.log("the cat tag " + cat_tag);

      // ASYNC OR SYNC ???
      cache.rpush(cat_tag, id2translation);

    }
  }

  // here if done synchronous task

  if (typeof callback == 'function') {

    callback(undefined);
  }

}

// categories in redis cache => livestream-categories-en
//                               [id:label,id:label ..]
// removes category with category id from store of categories for type streams
// in all enabled languages
function removeCategoryTranslationsFromCache(category_id, type,
  enabled_languages, callback) {
  var done = 0;
  // for each type-categories list by language
  for (var i = 0; i < enabled_languages.length; i++) {
    var lang = enabled_languages[i];
    var list_tag = type + "-categories-" + lang;
    console.log("the list tag " + list_tag);

    removeCategoryFromCacheList(category_id, list_tag, function() {
      done++;
      if (done == enabled_languages.length) {
        return callback();
      }
    });
  }
}

// search cache for category tag with id= category_id
// in list with name list_tag
// remove the categorY
function removeCategoryFromCacheList(category_id, list_tag, callback) {
  var done = 0;
  cache.lrange(list_tag, 0, -1, function(error, categories) {
    if (error) {
      console.log("error " + error);
      throw error
    }
    categories.forEach(function(category) {
      var cache_category_id = category.split(":")[0];
      console.log("cache category id " + cache_category_id);
      // if this cached category's id matches the id to remove
      if (category_id == cache_category_id) {
        console.log("removing");
        // remove from cache
        console.log("the category " + category + "the list tag " + list_tag);
        cache.lrem(list_tag, 0, category, function(err) {});
      }
      done++;
      if (done == categories.length) {
        return callback();
      }
    });
  });
}

// redis store: livestream-categories-en=> [id:label,id:label..]
function changeCachedCategory(type, category_id, new_label, language, callback) {
  var list_tag = type + '-categories-' + language;
  // formatted for redis store
  var new_cat_tag = category_id + ":" + new_label;

  cache.lrange(list_tag, 0, -1, function(error, categories) {
    if (error) {
      console.log("error " + error);
      throw error
    }
    categories.forEach(function(category) {
      var cache_category_id = category.split(":")[0];
      console.log("cache category id " + cache_category_id);
      // if this cached category's id matches the id to edit
      if (category_id == cache_category_id) {
        console.log("editing");
        // editing label
        cache.lrem(list_tag, 0, category, function(err) {
          cache.lpush(list_tag, new_cat_tag, function(err) {
            return callback();
          });
        });
      }
    });
  });
};

// pass a list of values, get the list of corresponding keys
// from obj
function listKeysForValues(obj, values_list) {
  var keys_list = [];
  for (var key in obj) {
    var value = obj[key];
    if (isInArray(value, values_list)) {
      keys_list.push(key);
    }
  }
  return keys_list;
}

function isInArray(value, array) {
  return array.indexOf(value) > -1;
}


// CHACHE FUNCT **


module.exports = router;