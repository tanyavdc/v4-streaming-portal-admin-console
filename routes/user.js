var express = require('express'),
  router = express.Router();



var model = require('../model/user.js');

var passport = require('passport');

var multer = require('multer');

var security = require('../middleware/security.js');

var sess;



var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './public/tmp/')
  },
  filename: function(req, file, cb) {
    cb(null, file.fieldname)
  }
})



var upload = multer({
  storage: storage
});


// GETS

router.get('/login', function(req, res) {
  // to home page where recently watched and favourites are displayed IF SIGNED ON AS CUSTOMER **

  sess = req.session;

  if (sess.user.logged_in) {

    res.redirect('/');
  } else {
    res.render('login', {
      title: 'Login',
      error: req.flash('loginMessage'),
      session: sess
    });
  }

});

router.get('/signup', function(req, res) {

  sess = req.session;

  res.render('signup', {
    title: 'Signup',
    session: sess,
    error: req.flash('loginMessage')
  });

});



router.get('/signout', function(req, res) {

  sess = req.session;

  // passport logout
  sess.user = {
    logged_in: false
  };
  req.logout();

  res.redirect('back');

});

router.get('/success', function(req, res) {
  sess = req.session;
  res.render('home', {
    msg: "Success",
    session: sess
  });


})


router.get('/settings', security.ensureAuthenticated, function(req, res) {
  sess = req.session;

  console.log("getting SETTINGS");


  // get lang translation welcome msgs form cache ** TODO
  // send to json to prepopulate form ** TODO

  model.getSettings(function(err, data) {
    // PASS SETINGS DATA *
    console.log("in get setttings the data is " + JSON.stringify(data))

    // ** req.app.locals.settings??
    res.render('settings', {
      title: 'settings',
      data: data,
      session: sess
    });
  });

});

// POSTS


router.post('/login', function(req, res) {

  var sess = req.session;

  // fake flash message because flash isnt working on redirect


  passport.authenticate('local-login',
    function(err, user, info) {

      // if login unsuccessful
      if (!sess.user.logged_in) {
        // back to login
        // because flash isnt working after redirect
        res.redirect('back');
      }

      // all is well
      else {
        req.logIn(user, function(err) { // <-- Log user in
          // get redirect path N
          if (sess.redirect_to) {

            var redirect_path = sess.redirect_to;
            sess.redirect_to = '';

            return res.redirect(redirect_path);
          }
          // no redirect path specified
          // go home
          else {
            return res.redirect('/');
          }
        });
      }
    })(req, res);
});

router.post('/signup', function(req, res) {

  sess = req.session;
  console.log("the stuff " + JSON.stringify(req.body));
  // hash password **
  var raw_password = req.body['password'];
  var hash = bcrypt.hashSync(req.body['password'], salt);

  var email = req.body['email'];

  model.signup(email, hash, function(err) {

    //if err
    // render err

    if (!err) {
      // log user in
      passport.authenticate('local-login')(req, res,
        function(err, user, info) {

          console.log("authenticated");

          res.render('success', {
            title: 'Sign Up',
            msg: "Signed up successfully",
            session: sess
          });
        });
    }

    // user exists
    else {
      res.status(400).send("User exists");
    }

  })
});



// translate page
router.post('/translate/:language', function(req, res) {

  var sess = req.session;
  sess.current_language = req.params.language;

  console.log("the session " + JSON.stringify(sess));
  res.redirect("back");
})


router.post('/settings', [security.ensureAuthenticated,
  sanitizer.formatFormStringsForSQL, upload.single('logo')
], function(req, res) {


  var default_language = config.default_language;
  var new_default_language = req.body.lang;

  sess = req.session;
  var enabled_services = req.body['enabled'];

  model.setSettings(req, function(err) {


    setLocals(req, enabled_services, function() { // ** TEMP **

      handleAddedLanguages(req, function(added) {
        //model.setWelcomeMsg
        // check for new language
        if (new_default_language != default_language) {
          console.log("changing language");
          model.changeDefaultLanguage(new_default_language, req, function() {

            res.render('home', {
              title: 'Settings',
              msg: 'Success',
              session: sess
            });
          });

        } else {
          res.render('home', {
            title: 'Settings',
            msg: 'Success',
            session: sess
          });
        }
      })
    })
  });
});



// **** HELPERS ******


// TEMP **
function setLocals(req, enabled_services, callback) {

  // set colours for paint to render immediately
  req.app.locals.settings["primary-colour"] = req.body["primary-colour"]
  req.app.locals.settings["secondary-colour"] = req.body["secondary-colour"]


  // if service is not in enabled services, than this service is to be disabled
  if (enabled_services.indexOf('livestream') < 0) {
    console.log("Disabling");
    req.app.locals.livestreams_enabled = false;

    // TO REDIS CACHE **


    // for lang in enabled langs
  } else {
    req.app.locals.livestreams_enabled = true;
  }

  if (enabled_services.indexOf('ondemand') < 0) {
    console.log("Disabling");

    req.app.locals.ondemand_enabled = false;



    // for lang in enabled langs
  } else {
    req.app.locals.ondemand_enabled = true;
  }


  if (enabled_services.indexOf('advertisement') < 0) {
    console.log("Disabling");
    req.app.locals.advertisements_enabled = false;


    // for lang in enabled langs
  } else {
    req.app.locals.advertisements_enabled = true;

  }
  if (enabled_services.indexOf('go') < 0) {
    console.log("Disabling");
    req.app.locals.goservices_enabled = false;


    // for lang in enabled langs
  } else {

    req.app.locals.goservices_enabled = true;

  }

  callback();
}
// false if no languages were added
function handleAddedLanguages(req, callback) {

  var default_language = config.default_language;


  var default_livestream_categories_tag = 'livestream-categories-' + default_language;
  var default_ondemand_categories_tag = 'ondemand-categories-' + default_language;
  var default_advertisement_categories_tag = 'advertisement-categories-' + default_language;



  if (req.body['added-lang'] == 'true') {

    var new_langs_raw = req.body['new-langs'];
    var new_langs = new_langs_raw.trim().split(' ');
    var enabled_langs = req.app.locals.enabled_languages;

    model.addNewLanguages(new_langs, enabled_langs, function() {



      model.setAndGetCachedCategories(new_langs, default_livestream_categories_tag, default_ondemand_categories_tag,
        default_advertisement_categories_tag,
        function(livestream_categories, ondemand_categories, ad_categories) {


          // add these categories to this language in db
          model.addCategoryTranslations(default_language, new_langs, 'livestream',
            livestream_categories,
            function() {

              model.addCategoryTranslations(default_language, new_langs, 'ondemand',
                ondemand_categories,
                function() {

                  model.addCategoryTranslations(default_language, new_langs, 'advertisement',
                    ad_categories,
                    function() {
                      callback(true)
                    });
                });
            });

        });
    });

  }
  callback(false)

}



module.exports = router;