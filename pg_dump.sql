--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.1
-- Dumped by pg_dump version 9.6.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: lang; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE lang AS ENUM (
    'en',
    'fr',
    'es-mx',
    'es'
);


ALTER TYPE lang OWNER TO postgres;

--
-- Name: payment_option; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE payment_option AS ENUM (
    'stripe',
    'account',
    'ip',
    'saml',
    'none'
);


ALTER TYPE payment_option OWNER TO postgres;

--
-- Name: service; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE service AS ENUM (
    'livestream',
    'ondemand',
    'advertisement',
    'go',
    'login',
    'account'
);


ALTER TYPE service OWNER TO postgres;

--
-- Name: stream_type; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE stream_type AS ENUM (
    'livestream',
    'ondemand',
    'advertisement'
);


ALTER TYPE stream_type OWNER TO postgres;

--
-- Name: subscription_plan; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE subscription_plan AS ENUM (
    'daily',
    'weekly',
    'monthly'
);


ALTER TYPE subscription_plan OWNER TO postgres;

--
-- Name: user_type; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE user_type AS ENUM (
    'admin',
    'customer',
    'user'
);


ALTER TYPE user_type OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE admin (
    id integer NOT NULL,
    password character varying
);


ALTER TABLE admin OWNER TO postgres;

--
-- Name: admin2domain; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE admin2domain (
    admin_id integer NOT NULL,
    domain_id integer NOT NULL
);


ALTER TABLE admin2domain OWNER TO postgres;

--
-- Name: admin2domain_admin_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE admin2domain_admin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin2domain_admin_id_seq OWNER TO postgres;

--
-- Name: admin2domain_admin_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE admin2domain_admin_id_seq OWNED BY admin2domain.admin_id;


--
-- Name: admin2domain_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE admin2domain_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin2domain_domain_id_seq OWNER TO postgres;

--
-- Name: admin2domain_domain_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE admin2domain_domain_id_seq OWNED BY admin2domain.domain_id;


--
-- Name: admin_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE admin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE admin_id_seq OWNER TO postgres;

--
-- Name: admin_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE admin_id_seq OWNED BY admin.id;


--
-- Name: advertisement; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE advertisement (
    id integer NOT NULL,
    video character varying,
    pubstart timestamp without time zone,
    pubend timestamp without time zone,
    thumbnail character varying
);


ALTER TABLE advertisement OWNER TO postgres;

--
-- Name: advertisement_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE advertisement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE advertisement_id_seq OWNER TO postgres;

--
-- Name: advertisement_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE advertisement_id_seq OWNED BY advertisement.id;


--
-- Name: authentication; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE authentication (
    id integer NOT NULL
);


ALTER TABLE authentication OWNER TO postgres;

--
-- Name: authentication_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE authentication_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authentication_id_seq OWNER TO postgres;

--
-- Name: authentication_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE authentication_id_seq OWNED BY authentication.id;


--
-- Name: category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE category (
    id integer NOT NULL,
    type stream_type
);


ALTER TABLE category OWNER TO postgres;

--
-- Name: category2lang; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE category2lang (
    id integer NOT NULL,
    language character varying(2),
    label character varying(30)
);


ALTER TABLE category2lang OWNER TO postgres;

--
-- Name: category2lang_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE category2lang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE category2lang_id_seq OWNER TO postgres;

--
-- Name: category2lang_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE category2lang_id_seq OWNED BY category2lang.id;


--
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE category_id_seq OWNER TO postgres;

--
-- Name: category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE category_id_seq OWNED BY category.id;


--
-- Name: credentials; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE credentials (
    last4digits character varying,
    credit_token character varying,
    stripe_id character varying,
    customer_id integer,
    subscription_plan character varying,
    expiry timestamp with time zone,
    subscription_id character varying
);


ALTER TABLE credentials OWNER TO postgres;

--
-- Name: customer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE customer (
    id integer NOT NULL
);


ALTER TABLE customer OWNER TO postgres;

--
-- Name: customer2favourites; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE customer2favourites (
    cust_id integer NOT NULL,
    stream_id integer NOT NULL
);


ALTER TABLE customer2favourites OWNER TO postgres;

--
-- Name: customer2favourites_cust_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE customer2favourites_cust_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer2favourites_cust_id_seq OWNER TO postgres;

--
-- Name: customer2favourites_cust_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE customer2favourites_cust_id_seq OWNED BY customer2favourites.cust_id;


--
-- Name: customer2favourites_stream_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE customer2favourites_stream_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer2favourites_stream_id_seq OWNER TO postgres;

--
-- Name: customer2favourites_stream_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE customer2favourites_stream_id_seq OWNED BY customer2favourites.stream_id;


--
-- Name: customer2watched; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE customer2watched (
    cust_id integer NOT NULL,
    stream_id integer NOT NULL,
    viewed_at timestamp without time zone DEFAULT now(),
    view_count integer DEFAULT 1
);


ALTER TABLE customer2watched OWNER TO postgres;

--
-- Name: customer2recentlywatched_cust_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE customer2recentlywatched_cust_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer2recentlywatched_cust_id_seq OWNER TO postgres;

--
-- Name: customer2recentlywatched_cust_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE customer2recentlywatched_cust_id_seq OWNED BY customer2watched.cust_id;


--
-- Name: customer2recentlywatched_stream_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE customer2recentlywatched_stream_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer2recentlywatched_stream_id_seq OWNER TO postgres;

--
-- Name: customer2recentlywatched_stream_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE customer2recentlywatched_stream_id_seq OWNED BY customer2watched.stream_id;


--
-- Name: customer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE customer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customer_id_seq OWNER TO postgres;

--
-- Name: customer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE customer_id_seq OWNED BY customer.id;


--
-- Name: domain; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE domain (
    id integer NOT NULL,
    name character varying,
    web_label character varying,
    web_url character varying,
    logo character varying,
    header character varying,
    s_colour character varying,
    p_colour integer
);


ALTER TABLE domain OWNER TO postgres;

--
-- Name: domain2authentication; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE domain2authentication (
    domain_id integer NOT NULL,
    authent_id integer NOT NULL
);


ALTER TABLE domain2authentication OWNER TO postgres;

--
-- Name: domain2authentication_authent_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE domain2authentication_authent_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE domain2authentication_authent_id_seq OWNER TO postgres;

--
-- Name: domain2authentication_authent_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE domain2authentication_authent_id_seq OWNED BY domain2authentication.authent_id;


--
-- Name: domain2authentication_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE domain2authentication_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE domain2authentication_domain_id_seq OWNER TO postgres;

--
-- Name: domain2authentication_domain_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE domain2authentication_domain_id_seq OWNED BY domain2authentication.domain_id;


--
-- Name: domain2lang; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE domain2lang (
    id integer NOT NULL,
    language lang,
    welcome_msg character varying,
    twitter_msg character varying,
    fb_msg character varying
);


ALTER TABLE domain2lang OWNER TO postgres;

--
-- Name: domain2lang_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE domain2lang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE domain2lang_id_seq OWNER TO postgres;

--
-- Name: domain2lang_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE domain2lang_id_seq OWNED BY domain2lang.id;


--
-- Name: domain_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE domain_id_seq OWNER TO postgres;

--
-- Name: domain_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE domain_id_seq OWNED BY domain.id;


--
-- Name: enabled_languages; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE enabled_languages (
    language character varying(2) NOT NULL
);


ALTER TABLE enabled_languages OWNER TO postgres;

--
-- Name: enabled_services; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE enabled_services (
    service service
);


ALTER TABLE enabled_services OWNER TO postgres;

--
-- Name: faq; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE faq (
    id integer NOT NULL,
    domain_id integer NOT NULL
);


ALTER TABLE faq OWNER TO postgres;

--
-- Name: faq2category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE faq2category (
    faq_id integer NOT NULL,
    category_id integer NOT NULL
);


ALTER TABLE faq2category OWNER TO postgres;

--
-- Name: faq2category_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE faq2category_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE faq2category_category_id_seq OWNER TO postgres;

--
-- Name: faq2category_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE faq2category_category_id_seq OWNED BY faq2category.category_id;


--
-- Name: faq2category_faq_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE faq2category_faq_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE faq2category_faq_id_seq OWNER TO postgres;

--
-- Name: faq2category_faq_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE faq2category_faq_id_seq OWNED BY faq2category.faq_id;


--
-- Name: faq2lang; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE faq2lang (
    id integer NOT NULL,
    language lang,
    question character varying,
    answer character varying
);


ALTER TABLE faq2lang OWNER TO postgres;

--
-- Name: faq2lang_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE faq2lang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE faq2lang_id_seq OWNER TO postgres;

--
-- Name: faq2lang_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE faq2lang_id_seq OWNED BY faq2lang.id;


--
-- Name: faq_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE faq_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE faq_domain_id_seq OWNER TO postgres;

--
-- Name: faq_domain_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE faq_domain_id_seq OWNED BY faq.domain_id;


--
-- Name: faq_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE faq_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE faq_id_seq OWNER TO postgres;

--
-- Name: faq_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE faq_id_seq OWNED BY faq.id;


--
-- Name: faqcategory; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE faqcategory (
    id integer NOT NULL
);


ALTER TABLE faqcategory OWNER TO postgres;

--
-- Name: faqcategory2lang; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE faqcategory2lang (
    id integer NOT NULL,
    language lang,
    label character varying
);


ALTER TABLE faqcategory2lang OWNER TO postgres;

--
-- Name: faqcategory2lang_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE faqcategory2lang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE faqcategory2lang_id_seq OWNER TO postgres;

--
-- Name: faqcategory2lang_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE faqcategory2lang_id_seq OWNED BY faqcategory2lang.id;


--
-- Name: faqcategory_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE faqcategory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE faqcategory_id_seq OWNER TO postgres;

--
-- Name: faqcategory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE faqcategory_id_seq OWNED BY faqcategory.id;


--
-- Name: goservice; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE goservice (
    id integer NOT NULL,
    link character varying,
    thumbnail character varying,
    title character varying(50)
);


ALTER TABLE goservice OWNER TO postgres;

--
-- Name: goservice_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE goservice_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE goservice_id_seq OWNER TO postgres;

--
-- Name: goservice_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE goservice_id_seq OWNED BY goservice.id;


--
-- Name: livestream; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE livestream (
    id integer NOT NULL,
    port integer,
    hostname character varying,
    thumbnail character varying,
    rtmp_protocol character varying(6),
    m3u8_protocol character varying(6),
    application character varying,
    video character varying,
    streamname character varying,
    CONSTRAINT check_m3u8 CHECK ((((m3u8_protocol)::text = 'http'::text) OR ((m3u8_protocol)::text = 'https'::text))),
    CONSTRAINT check_rtmp CHECK ((((rtmp_protocol)::text = 'rtmp'::text) OR ((rtmp_protocol)::text = 'rtmps'::text)))
);


ALTER TABLE livestream OWNER TO postgres;

--
-- Name: livestream_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE livestream_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE livestream_id_seq OWNER TO postgres;

--
-- Name: livestream_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE livestream_id_seq OWNED BY livestream.id;


--
-- Name: ondemand; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE ondemand (
    id integer NOT NULL,
    video character varying
);


ALTER TABLE ondemand OWNER TO postgres;

--
-- Name: ondemand_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ondemand_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ondemand_id_seq OWNER TO postgres;

--
-- Name: ondemand_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ondemand_id_seq OWNED BY ondemand.id;


--
-- Name: saml; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE saml (
    id integer NOT NULL
);


ALTER TABLE saml OWNER TO postgres;

--
-- Name: saml_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE saml_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE saml_id_seq OWNER TO postgres;

--
-- Name: saml_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE saml_id_seq OWNED BY saml.id;


--
-- Name: settings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE settings (
    website_label character varying,
    website_url character varying,
    logo character varying,
    fb_api_token character varying,
    fb_page_id character varying,
    primary_colour character varying,
    secondary_colour character varying,
    payment_gateway payment_option,
    default_language character varying(2)
);


ALTER TABLE settings OWNER TO postgres;

--
-- Name: settings2lang; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE settings2lang (
    language character varying(2),
    homepage_msg character varying
);


ALTER TABLE settings2lang OWNER TO postgres;

--
-- Name: stream; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE stream (
    id integer NOT NULL,
    type stream_type,
    thumbnail character varying(500),
    private boolean DEFAULT false,
    featured boolean DEFAULT false,
    pub_start date DEFAULT now(),
    pub_end date,
    source character varying,
    poster character varying,
    filetype character varying
);


ALTER TABLE stream OWNER TO postgres;

--
-- Name: stream2ad; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE stream2ad (
    stream_id integer NOT NULL,
    ad_id integer NOT NULL
);


ALTER TABLE stream2ad OWNER TO postgres;

--
-- Name: stream2ad_ad_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE stream2ad_ad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE stream2ad_ad_id_seq OWNER TO postgres;

--
-- Name: stream2ad_ad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE stream2ad_ad_id_seq OWNED BY stream2ad.ad_id;


--
-- Name: stream2ad_stream_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE stream2ad_stream_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE stream2ad_stream_id_seq OWNER TO postgres;

--
-- Name: stream2ad_stream_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE stream2ad_stream_id_seq OWNED BY stream2ad.stream_id;


--
-- Name: stream2category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE stream2category (
    stream_id integer NOT NULL,
    category_id integer NOT NULL
);


ALTER TABLE stream2category OWNER TO postgres;

--
-- Name: stream2category_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE stream2category_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE stream2category_category_id_seq OWNER TO postgres;

--
-- Name: stream2category_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE stream2category_category_id_seq OWNED BY stream2category.category_id;


--
-- Name: stream2category_stream_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE stream2category_stream_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE stream2category_stream_id_seq OWNER TO postgres;

--
-- Name: stream2category_stream_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE stream2category_stream_id_seq OWNED BY stream2category.stream_id;


--
-- Name: stream2lang; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE stream2lang (
    id integer NOT NULL,
    language character varying(2),
    title character varying,
    description character varying
);


ALTER TABLE stream2lang OWNER TO postgres;

--
-- Name: stream2lang_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE stream2lang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE stream2lang_id_seq OWNER TO postgres;

--
-- Name: stream2lang_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE stream2lang_id_seq OWNED BY stream2lang.id;


--
-- Name: stream2tag; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE stream2tag (
    stream_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE stream2tag OWNER TO postgres;

--
-- Name: stream2tag_stream_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE stream2tag_stream_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE stream2tag_stream_id_seq OWNER TO postgres;

--
-- Name: stream2tag_stream_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE stream2tag_stream_id_seq OWNED BY stream2tag.stream_id;


--
-- Name: stream2tag_tag_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE stream2tag_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE stream2tag_tag_id_seq OWNER TO postgres;

--
-- Name: stream2tag_tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE stream2tag_tag_id_seq OWNED BY stream2tag.tag_id;


--
-- Name: stream_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE stream_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE stream_id_seq OWNER TO postgres;

--
-- Name: stream_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE stream_id_seq OWNED BY stream.id;


--
-- Name: stripe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE stripe (
    id integer NOT NULL,
    public_api_key character varying,
    private_api_key character varying
);


ALTER TABLE stripe OWNER TO postgres;

--
-- Name: stripe_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE stripe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE stripe_id_seq OWNER TO postgres;

--
-- Name: stripe_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE stripe_id_seq OWNED BY stripe.id;


--
-- Name: tag; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tag (
    id integer NOT NULL,
    label character varying
);


ALTER TABLE tag OWNER TO postgres;

--
-- Name: tag2lang; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tag2lang (
    id integer NOT NULL,
    language character varying(2),
    label character varying
);


ALTER TABLE tag2lang OWNER TO postgres;

--
-- Name: tag2lang_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tag2lang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tag2lang_id_seq OWNER TO postgres;

--
-- Name: tag2lang_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tag2lang_id_seq OWNED BY tag2lang.id;


--
-- Name: tag_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tag_id_seq OWNER TO postgres;

--
-- Name: tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tag_id_seq OWNED BY tag.id;


--
-- Name: user2favourites; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE user2favourites (
    user_id integer,
    stream_id integer
);


ALTER TABLE user2favourites OWNER TO postgres;

--
-- Name: user2watched; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE user2watched (
    user_id integer,
    stream_id integer,
    viewed_at timestamp without time zone DEFAULT now(),
    view_count integer DEFAULT 1
);


ALTER TABLE user2watched OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE users (
    id integer NOT NULL,
    email character varying,
    type user_type,
    password character varying
);


ALTER TABLE users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: votes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE votes (
    stream_id integer NOT NULL,
    vote integer DEFAULT 0,
    user_id integer,
    CONSTRAINT vote_check CHECK (((vote <= 1) AND (vote >= '-1'::integer)))
);


ALTER TABLE votes OWNER TO postgres;

--
-- Name: votes_stream_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE votes_stream_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE votes_stream_id_seq OWNER TO postgres;

--
-- Name: votes_stream_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE votes_stream_id_seq OWNED BY votes.stream_id;


--
-- Name: admin id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin ALTER COLUMN id SET DEFAULT nextval('admin_id_seq'::regclass);


--
-- Name: admin2domain admin_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin2domain ALTER COLUMN admin_id SET DEFAULT nextval('admin2domain_admin_id_seq'::regclass);


--
-- Name: admin2domain domain_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin2domain ALTER COLUMN domain_id SET DEFAULT nextval('admin2domain_domain_id_seq'::regclass);


--
-- Name: advertisement id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY advertisement ALTER COLUMN id SET DEFAULT nextval('advertisement_id_seq'::regclass);


--
-- Name: authentication id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY authentication ALTER COLUMN id SET DEFAULT nextval('authentication_id_seq'::regclass);


--
-- Name: category id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY category ALTER COLUMN id SET DEFAULT nextval('category_id_seq'::regclass);


--
-- Name: category2lang id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY category2lang ALTER COLUMN id SET DEFAULT nextval('category2lang_id_seq'::regclass);


--
-- Name: customer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer ALTER COLUMN id SET DEFAULT nextval('customer_id_seq'::regclass);


--
-- Name: customer2favourites cust_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer2favourites ALTER COLUMN cust_id SET DEFAULT nextval('customer2favourites_cust_id_seq'::regclass);


--
-- Name: customer2favourites stream_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer2favourites ALTER COLUMN stream_id SET DEFAULT nextval('customer2favourites_stream_id_seq'::regclass);


--
-- Name: customer2watched cust_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer2watched ALTER COLUMN cust_id SET DEFAULT nextval('customer2recentlywatched_cust_id_seq'::regclass);


--
-- Name: customer2watched stream_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer2watched ALTER COLUMN stream_id SET DEFAULT nextval('customer2recentlywatched_stream_id_seq'::regclass);


--
-- Name: domain id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY domain ALTER COLUMN id SET DEFAULT nextval('domain_id_seq'::regclass);


--
-- Name: domain2authentication domain_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY domain2authentication ALTER COLUMN domain_id SET DEFAULT nextval('domain2authentication_domain_id_seq'::regclass);


--
-- Name: domain2authentication authent_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY domain2authentication ALTER COLUMN authent_id SET DEFAULT nextval('domain2authentication_authent_id_seq'::regclass);


--
-- Name: domain2lang id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY domain2lang ALTER COLUMN id SET DEFAULT nextval('domain2lang_id_seq'::regclass);


--
-- Name: faq id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY faq ALTER COLUMN id SET DEFAULT nextval('faq_id_seq'::regclass);


--
-- Name: faq domain_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY faq ALTER COLUMN domain_id SET DEFAULT nextval('faq_domain_id_seq'::regclass);


--
-- Name: faq2category faq_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY faq2category ALTER COLUMN faq_id SET DEFAULT nextval('faq2category_faq_id_seq'::regclass);


--
-- Name: faq2category category_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY faq2category ALTER COLUMN category_id SET DEFAULT nextval('faq2category_category_id_seq'::regclass);


--
-- Name: faq2lang id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY faq2lang ALTER COLUMN id SET DEFAULT nextval('faq2lang_id_seq'::regclass);


--
-- Name: faqcategory id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY faqcategory ALTER COLUMN id SET DEFAULT nextval('faqcategory_id_seq'::regclass);


--
-- Name: faqcategory2lang id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY faqcategory2lang ALTER COLUMN id SET DEFAULT nextval('faqcategory2lang_id_seq'::regclass);


--
-- Name: goservice id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY goservice ALTER COLUMN id SET DEFAULT nextval('goservice_id_seq'::regclass);


--
-- Name: livestream id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY livestream ALTER COLUMN id SET DEFAULT nextval('livestream_id_seq'::regclass);


--
-- Name: ondemand id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ondemand ALTER COLUMN id SET DEFAULT nextval('ondemand_id_seq'::regclass);


--
-- Name: saml id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY saml ALTER COLUMN id SET DEFAULT nextval('saml_id_seq'::regclass);


--
-- Name: stream id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stream ALTER COLUMN id SET DEFAULT nextval('stream_id_seq'::regclass);


--
-- Name: stream2ad stream_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stream2ad ALTER COLUMN stream_id SET DEFAULT nextval('stream2ad_stream_id_seq'::regclass);


--
-- Name: stream2ad ad_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stream2ad ALTER COLUMN ad_id SET DEFAULT nextval('stream2ad_ad_id_seq'::regclass);


--
-- Name: stream2category stream_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stream2category ALTER COLUMN stream_id SET DEFAULT nextval('stream2category_stream_id_seq'::regclass);


--
-- Name: stream2category category_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stream2category ALTER COLUMN category_id SET DEFAULT nextval('stream2category_category_id_seq'::regclass);


--
-- Name: stream2lang id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stream2lang ALTER COLUMN id SET DEFAULT nextval('stream2lang_id_seq'::regclass);


--
-- Name: stream2tag stream_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stream2tag ALTER COLUMN stream_id SET DEFAULT nextval('stream2tag_stream_id_seq'::regclass);


--
-- Name: stream2tag tag_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stream2tag ALTER COLUMN tag_id SET DEFAULT nextval('stream2tag_tag_id_seq'::regclass);


--
-- Name: stripe id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stripe ALTER COLUMN id SET DEFAULT nextval('stripe_id_seq'::regclass);


--
-- Name: tag id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tag ALTER COLUMN id SET DEFAULT nextval('tag_id_seq'::regclass);


--
-- Name: tag2lang id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tag2lang ALTER COLUMN id SET DEFAULT nextval('tag2lang_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: votes stream_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY votes ALTER COLUMN stream_id SET DEFAULT nextval('votes_stream_id_seq'::regclass);


--
-- Data for Name: admin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY admin (id, password) FROM stdin;
56	test
\.


--
-- Data for Name: admin2domain; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY admin2domain (admin_id, domain_id) FROM stdin;
\.


--
-- Name: admin2domain_admin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('admin2domain_admin_id_seq', 1, false);


--
-- Name: admin2domain_domain_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('admin2domain_domain_id_seq', 1, false);


--
-- Name: admin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('admin_id_seq', 1, false);


--
-- Data for Name: advertisement; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY advertisement (id, video, pubstart, pubend, thumbnail) FROM stdin;
500	/tmp/videos/adVid1485986897718.mp4	\N	\N	\N
505	/tmp/videos/adVid1485987179778.mp4	\N	\N	\N
\.


--
-- Name: advertisement_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('advertisement_id_seq', 1, false);


--
-- Data for Name: authentication; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY authentication (id) FROM stdin;
\.


--
-- Name: authentication_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('authentication_id_seq', 1, false);


--
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY category (id, type) FROM stdin;
600	advertisement
601	livestream
604	livestream
605	advertisement
608	ondemand
609	ondemand
610	ondemand
\.


--
-- Data for Name: category2lang; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY category2lang (id, language, label) FROM stdin;
600	en	Sponsors
600	fr	Sponsors
601	en	French Culture
601	fr	Culture Francais
604	en	News
604	fr	Nouvelles
605	en	Products
605	fr	Produits
608	en	News
608	fr	Nouvelles
609	en	Music
609	fr	Musique
610	en	Tv Programs
610	fr	Programmes
\.


--
-- Name: category2lang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('category2lang_id_seq', 1, false);


--
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('category_id_seq', 610, true);


--
-- Data for Name: credentials; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY credentials (last4digits, credit_token, stripe_id, customer_id, subscription_plan, expiry, subscription_id) FROM stdin;
4242	card_19hgPeFvpbW3Z5YJ0rC4hQyk	cus_A1jtilAhH3eDCY	57	monthly	2017-02-28 09:45:00-05	sub_A1jtMNEm6Nyuxe
4242	card_19hmvWFvpbW3Z5YJTNKfb14i	cus_A1qcUFprZSvnnd	58	yearly	2018-01-30 16:43:00-05	sub_A1qcbaBUQVDErk
\.


--
-- Data for Name: customer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY customer (id) FROM stdin;
\.


--
-- Data for Name: customer2favourites; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY customer2favourites (cust_id, stream_id) FROM stdin;
\.


--
-- Name: customer2favourites_cust_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('customer2favourites_cust_id_seq', 1, false);


--
-- Name: customer2favourites_stream_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('customer2favourites_stream_id_seq', 1, false);


--
-- Name: customer2recentlywatched_cust_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('customer2recentlywatched_cust_id_seq', 1, false);


--
-- Name: customer2recentlywatched_stream_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('customer2recentlywatched_stream_id_seq', 1, false);


--
-- Data for Name: customer2watched; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY customer2watched (cust_id, stream_id, viewed_at, view_count) FROM stdin;
\.


--
-- Name: customer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('customer_id_seq', 1, false);


--
-- Data for Name: domain; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY domain (id, name, web_label, web_url, logo, header, s_colour, p_colour) FROM stdin;
1	test domain1	test domain1 label	http://domain1.com	\N	\N	\N	\N
4	test domain4	test domain4 label	http://domain4.com	\N	\N	\N	\N
\.


--
-- Data for Name: domain2authentication; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY domain2authentication (domain_id, authent_id) FROM stdin;
\.


--
-- Name: domain2authentication_authent_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('domain2authentication_authent_id_seq', 1, false);


--
-- Name: domain2authentication_domain_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('domain2authentication_domain_id_seq', 1, false);


--
-- Data for Name: domain2lang; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY domain2lang (id, language, welcome_msg, twitter_msg, fb_msg) FROM stdin;
1	fr	bonjour, cest domain1 en francais	\N	\N
4	en	hey its domain4. we only serve english speakers here	\N	\N
1	en	hello this is domain1 in english	\N	\N
\.


--
-- Name: domain2lang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('domain2lang_id_seq', 1, false);


--
-- Name: domain_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('domain_id_seq', 4, true);


--
-- Data for Name: enabled_languages; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY enabled_languages (language) FROM stdin;
en
fr
sp
\.


--
-- Data for Name: enabled_services; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY enabled_services (service) FROM stdin;
livestream
ondemand
livestream
ondemand
\.


--
-- Data for Name: faq; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY faq (id, domain_id) FROM stdin;
\.


--
-- Data for Name: faq2category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY faq2category (faq_id, category_id) FROM stdin;
\.


--
-- Name: faq2category_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('faq2category_category_id_seq', 1, false);


--
-- Name: faq2category_faq_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('faq2category_faq_id_seq', 1, false);


--
-- Data for Name: faq2lang; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY faq2lang (id, language, question, answer) FROM stdin;
\.


--
-- Name: faq2lang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('faq2lang_id_seq', 1, false);


--
-- Name: faq_domain_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('faq_domain_id_seq', 1, false);


--
-- Name: faq_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('faq_id_seq', 1, false);


--
-- Data for Name: faqcategory; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY faqcategory (id) FROM stdin;
\.


--
-- Data for Name: faqcategory2lang; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY faqcategory2lang (id, language, label) FROM stdin;
\.


--
-- Name: faqcategory2lang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('faqcategory2lang_id_seq', 1, false);


--
-- Name: faqcategory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('faqcategory_id_seq', 1, false);


--
-- Data for Name: goservice; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY goservice (id, link, thumbnail, title) FROM stdin;
10	http://netflix.com	/tmp/thumbs/goserviceThumb1485802935156	Netflix
11	http://cravetv.ca	/tmp/thumbs/goserviceThumb1485802961429	CraveTV
\.


--
-- Name: goservice_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('goservice_id_seq', 11, true);


--
-- Data for Name: livestream; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY livestream (id, port, hostname, thumbnail, rtmp_protocol, m3u8_protocol, application, video, streamname) FROM stdin;
501	443	57d9353274103.streamlock.net	\N	rtmps	http	ccaplive	https://57d9353274103.streamlock.net:443/ccaplive/smil:ccaptv.smil/playlist.m3u8?076719eb414e424astarttime=1485986981278&076719eb414e424aendtime=1486001381279&076719eb414e424ahash=C3O_XVgfoXQ7deEgTQxZerxAmruIjr2NNFrKRu1uPQw=	ccaptv
502	443	57d9353274103.streamlock.net	\N	rtmps	http	icilive	https://57d9353274103.streamlock.net:443/icilive/smil:icitv.smil/playlist.m3u8?076719eb414e424astarttime=1485987049407&076719eb414e424aendtime=1486001449408&076719eb414e424ahash=FZM5Lhrx4mB2E33GT8A20zhpcy0hr9yMSYD4mGr27sQ=	icitv
\.


--
-- Name: livestream_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('livestream_id_seq', 1, false);


--
-- Data for Name: ondemand; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ondemand (id, video) FROM stdin;
504	/tmp/videos/vodVid1485987115072.mp4
507	/tmp/videos/vodVid1485987249169.mp4
509	/tmp/videos/vodVid1485987884250.mp4
\.


--
-- Name: ondemand_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ondemand_id_seq', 1, false);


--
-- Data for Name: saml; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY saml (id) FROM stdin;
\.


--
-- Name: saml_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('saml_id_seq', 1, false);


--
-- Data for Name: settings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY settings (website_label, website_url, logo, fb_api_token, fb_page_id, primary_colour, secondary_colour, payment_gateway, default_language) FROM stdin;
test label edited	tesasdfa		fas3rea	2fda	#ffffff	#ff2bea	ip	en
test label edited	tesasdfa		fas3rea	2fda	#ffffff	#ff2bea	ip	en
\.


--
-- Data for Name: settings2lang; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY settings2lang (language, homepage_msg) FROM stdin;
en	test en
fr	test fr
sp	tes sp
en	test en
fr	test fr
sp	tes sp
\.


--
-- Data for Name: stream; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY stream (id, type, thumbnail, private, featured, pub_start, pub_end, source, poster, filetype) FROM stdin;
500	advertisement	/tmp/thumbs/adThumb1485986917027	f	f	2017-02-01	\N	\N	\N	\N
501	livestream	/tmp/thumbs/livestreamThumb1485986966588	f	t	\N	\N	\N	\N	\N
502	livestream	/tmp/thumbs/livestreamThumb1485987028942	t	f	\N	\N	\N	\N	\N
504	ondemand	/tmp/thumbs/vodThumb1485987110827	f	f	\N	\N	\N	\N	\N
505	advertisement	/tmp/thumbs/default-thumbnail.jpg	f	f	2017-02-01	\N	\N	\N	\N
507	ondemand	/tmp/thumbs/vodThumb1485987254240	t	f	2017-02-12	\N	\N	\N	\N
509	ondemand	/tmp/thumbs/default-thumbnail.jpg	f	f	\N	\N	\N	\N	\N
\.


--
-- Data for Name: stream2ad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY stream2ad (stream_id, ad_id) FROM stdin;
501	500
507	505
\.


--
-- Name: stream2ad_ad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('stream2ad_ad_id_seq', 1, false);


--
-- Name: stream2ad_stream_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('stream2ad_stream_id_seq', 1, false);


--
-- Data for Name: stream2category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY stream2category (stream_id, category_id) FROM stdin;
500	600
501	601
501	604
502	601
504	608
505	605
507	609
509	610
\.


--
-- Name: stream2category_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('stream2category_category_id_seq', 1, false);


--
-- Name: stream2category_stream_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('stream2category_stream_id_seq', 1, false);


--
-- Data for Name: stream2lang; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY stream2lang (id, language, title, description) FROM stdin;
500	en	Piracy is a Crime	You wouldn't steal a car
500	fr	Piracy c'est une crime	
501	en	CCAP	Live from Quebec
501	fr	CCAP	Vive du Quebec
502	en	ICI	Live from France
502	fr		
504	en	Roller Coaster Crash	Fun with roller coaster tycoon
504	fr	Crash du Roller Coaster	C'est la Vie
505	en	Hot Bod Man Body Spray	
505	fr	Hot Bod Eau de Toilette	
507	en	Music video of the Year	TestTV proudly presents (after a word from our sponsors)
507	fr	Video Du Musique Du Anne	
509	en	Dr Phil	Cash me ouside
509	fr	Dr Phile	Cash me ouside
\.


--
-- Name: stream2lang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('stream2lang_id_seq', 1, false);


--
-- Data for Name: stream2tag; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY stream2tag (stream_id, tag_id) FROM stdin;
500	95
500	105
501	83
501	98
501	106
502	83
502	106
502	96
504	104
504	100
505	101
507	104
507	107
509	104
\.


--
-- Name: stream2tag_stream_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('stream2tag_stream_id_seq', 1, false);


--
-- Name: stream2tag_tag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('stream2tag_tag_id_seq', 1, false);


--
-- Name: stream_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('stream_id_seq', 511, true);


--
-- Data for Name: stripe; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY stripe (id, public_api_key, private_api_key) FROM stdin;
\.


--
-- Name: stripe_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('stripe_id_seq', 1, false);


--
-- Data for Name: tag; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tag (id, label) FROM stdin;
68	things
69	stuff
70	news
71	hello
72	test
73	tset
74	priacy
75	crime
76	health
77	wellbeing
78	parfume
79	hot
80	bod
81	crashes
82	local
83	french
84	body
85	wellness
86	rick
87	roll
88	silly
89	memes
90	TEST
91	something
92	elese
93	''''''test
94	tes
95	psa
96	france
97	quebec
98	canadian
99	rollercoaster
100	games
101	men
102	music
103	video
104	entertainment
105	piracy
106	life
107	rickroll
\.


--
-- Data for Name: tag2lang; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tag2lang (id, language, label) FROM stdin;
\.


--
-- Name: tag2lang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tag2lang_id_seq', 1, false);


--
-- Name: tag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tag_id_seq', 107, true);


--
-- Data for Name: user2favourites; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY user2favourites (user_id, stream_id) FROM stdin;
\.


--
-- Data for Name: user2watched; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY user2watched (user_id, stream_id, viewed_at, view_count) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users (id, email, type, password) FROM stdin;
56	test	admin	test
57	test@test.ca	customer	\N
58	tanya@hifyre.com	customer	\N
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 58, true);


--
-- Data for Name: votes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY votes (stream_id, vote, user_id) FROM stdin;
\.


--
-- Name: votes_stream_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('votes_stream_id_seq', 1, false);


--
-- Name: admin admin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin
    ADD CONSTRAINT admin_pkey PRIMARY KEY (id);


--
-- Name: authentication authentication_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY authentication
    ADD CONSTRAINT authentication_pkey PRIMARY KEY (id);


--
-- Name: category category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- Name: customer2favourites customer2favourites_cust_id_stream_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer2favourites
    ADD CONSTRAINT customer2favourites_cust_id_stream_id_key UNIQUE (cust_id, stream_id);


--
-- Name: customer customer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (id);


--
-- Name: domain domain_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY domain
    ADD CONSTRAINT domain_name_key UNIQUE (name);


--
-- Name: domain domain_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY domain
    ADD CONSTRAINT domain_pkey PRIMARY KEY (id);


--
-- Name: domain domain_web_url_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY domain
    ADD CONSTRAINT domain_web_url_key UNIQUE (web_url);


--
-- Name: enabled_languages enabled_languages_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY enabled_languages
    ADD CONSTRAINT enabled_languages_pkey PRIMARY KEY (language);


--
-- Name: faq2lang faq2lang_question_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY faq2lang
    ADD CONSTRAINT faq2lang_question_key UNIQUE (question);


--
-- Name: faq faq_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY faq
    ADD CONSTRAINT faq_pkey PRIMARY KEY (id);


--
-- Name: faqcategory2lang faqcategory2lang_label_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY faqcategory2lang
    ADD CONSTRAINT faqcategory2lang_label_key UNIQUE (label);


--
-- Name: faqcategory faqcategory_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY faqcategory
    ADD CONSTRAINT faqcategory_pkey PRIMARY KEY (id);


--
-- Name: stream stream_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stream
    ADD CONSTRAINT stream_pkey PRIMARY KEY (id);


--
-- Name: tag2lang tag2lang_label_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tag2lang
    ADD CONSTRAINT tag2lang_label_key UNIQUE (label);


--
-- Name: tag tag_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tag
    ADD CONSTRAINT tag_pkey PRIMARY KEY (id);


--
-- Name: users users_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_key UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: admin2domain admin2domain_admin_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin2domain
    ADD CONSTRAINT admin2domain_admin_id_fkey FOREIGN KEY (admin_id) REFERENCES admin(id);


--
-- Name: admin2domain admin2domain_domain_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin2domain
    ADD CONSTRAINT admin2domain_domain_id_fkey FOREIGN KEY (domain_id) REFERENCES domain(id);


--
-- Name: admin admin_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY admin
    ADD CONSTRAINT admin_id_fkey FOREIGN KEY (id) REFERENCES users(id);


--
-- Name: advertisement advertisement_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY advertisement
    ADD CONSTRAINT advertisement_id_fkey FOREIGN KEY (id) REFERENCES stream(id);


--
-- Name: category2lang category2lang_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY category2lang
    ADD CONSTRAINT category2lang_id_fkey FOREIGN KEY (id) REFERENCES category(id);


--
-- Name: credentials credentials_customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY credentials
    ADD CONSTRAINT credentials_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES users(id);


--
-- Name: customer2favourites customer2favourites_cust_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer2favourites
    ADD CONSTRAINT customer2favourites_cust_id_fkey FOREIGN KEY (cust_id) REFERENCES customer(id);


--
-- Name: customer2favourites customer2favourites_stream_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer2favourites
    ADD CONSTRAINT customer2favourites_stream_id_fkey FOREIGN KEY (stream_id) REFERENCES stream(id);


--
-- Name: customer2watched customer2recentlywatched_cust_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer2watched
    ADD CONSTRAINT customer2recentlywatched_cust_id_fkey FOREIGN KEY (cust_id) REFERENCES customer(id);


--
-- Name: customer2watched customer2recentlywatched_stream_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer2watched
    ADD CONSTRAINT customer2recentlywatched_stream_id_fkey FOREIGN KEY (stream_id) REFERENCES stream(id);


--
-- Name: customer customer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_id_fkey FOREIGN KEY (id) REFERENCES users(id);


--
-- Name: domain2authentication domain2authentication_authent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY domain2authentication
    ADD CONSTRAINT domain2authentication_authent_id_fkey FOREIGN KEY (authent_id) REFERENCES authentication(id);


--
-- Name: domain2authentication domain2authentication_domain_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY domain2authentication
    ADD CONSTRAINT domain2authentication_domain_id_fkey FOREIGN KEY (domain_id) REFERENCES domain(id);


--
-- Name: domain2lang domain2lang_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY domain2lang
    ADD CONSTRAINT domain2lang_id_fkey FOREIGN KEY (id) REFERENCES domain(id);


--
-- Name: faq2category faq2category_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY faq2category
    ADD CONSTRAINT faq2category_category_id_fkey FOREIGN KEY (category_id) REFERENCES faqcategory(id);


--
-- Name: faq2category faq2category_faq_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY faq2category
    ADD CONSTRAINT faq2category_faq_id_fkey FOREIGN KEY (faq_id) REFERENCES faq(id);


--
-- Name: faq2lang faq2lang_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY faq2lang
    ADD CONSTRAINT faq2lang_id_fkey FOREIGN KEY (id) REFERENCES faq(id);


--
-- Name: faq faq_domain_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY faq
    ADD CONSTRAINT faq_domain_id_fkey FOREIGN KEY (domain_id) REFERENCES domain(id);


--
-- Name: faqcategory2lang faqcategory2lang_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY faqcategory2lang
    ADD CONSTRAINT faqcategory2lang_id_fkey FOREIGN KEY (id) REFERENCES faqcategory(id);


--
-- Name: livestream livestream_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY livestream
    ADD CONSTRAINT livestream_id_fkey FOREIGN KEY (id) REFERENCES stream(id);


--
-- Name: ondemand ondemand_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ondemand
    ADD CONSTRAINT ondemand_id_fkey FOREIGN KEY (id) REFERENCES stream(id);


--
-- Name: saml saml_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY saml
    ADD CONSTRAINT saml_id_fkey FOREIGN KEY (id) REFERENCES authentication(id);


--
-- Name: stream2ad stream2ad_ad_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stream2ad
    ADD CONSTRAINT stream2ad_ad_id_fkey FOREIGN KEY (ad_id) REFERENCES stream(id);


--
-- Name: stream2ad stream2ad_stream_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stream2ad
    ADD CONSTRAINT stream2ad_stream_id_fkey FOREIGN KEY (stream_id) REFERENCES stream(id);


--
-- Name: stream2category stream2category_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stream2category
    ADD CONSTRAINT stream2category_category_id_fkey FOREIGN KEY (category_id) REFERENCES category(id);


--
-- Name: stream2category stream2category_stream_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stream2category
    ADD CONSTRAINT stream2category_stream_id_fkey FOREIGN KEY (stream_id) REFERENCES stream(id);


--
-- Name: stream2lang stream2lang_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stream2lang
    ADD CONSTRAINT stream2lang_id_fkey FOREIGN KEY (id) REFERENCES stream(id);


--
-- Name: stream2tag stream2tag_stream_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stream2tag
    ADD CONSTRAINT stream2tag_stream_id_fkey FOREIGN KEY (stream_id) REFERENCES stream(id);


--
-- Name: stream2tag stream2tag_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stream2tag
    ADD CONSTRAINT stream2tag_tag_id_fkey FOREIGN KEY (tag_id) REFERENCES tag(id);


--
-- Name: stripe stripe_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stripe
    ADD CONSTRAINT stripe_id_fkey FOREIGN KEY (id) REFERENCES authentication(id);


--
-- Name: tag2lang tag2lang_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tag2lang
    ADD CONSTRAINT tag2lang_id_fkey FOREIGN KEY (id) REFERENCES tag(id);


--
-- Name: user2favourites user2favourites_stream_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user2favourites
    ADD CONSTRAINT user2favourites_stream_id_fkey FOREIGN KEY (stream_id) REFERENCES stream(id);


--
-- Name: user2favourites user2favourites_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user2favourites
    ADD CONSTRAINT user2favourites_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: user2watched user2watched_stream_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user2watched
    ADD CONSTRAINT user2watched_stream_id_fkey FOREIGN KEY (stream_id) REFERENCES stream(id);


--
-- Name: user2watched user2watched_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user2watched
    ADD CONSTRAINT user2watched_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: votes votes_stream_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY votes
    ADD CONSTRAINT votes_stream_id_fkey FOREIGN KEY (stream_id) REFERENCES stream(id);


--
-- Name: votes votes_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY votes
    ADD CONSTRAINT votes_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- PostgreSQL database dump complete
--

