// set colours
function paint(color_primary, color_secondary) {

  if(color_primary == '#ffffff'){
    color_primary = ColorLuminance(color_primary, -0.1);
  }
  if(color_secondary == '#ffffff'){
    color_secondary = ColorLuminance(color_secondary, -0.1);
  }

  var color_secondary_darken_40 = ColorLuminance(color_secondary, -0.4);
  var color_primary_lighten_15 = ColorLuminance(color_primary, 0.85);
  var color_primary_darken_10 = ColorLuminance(color_primary, -0.1);

  $.get("/stylesheets/main.css", function(data) {

    var mapObj = {
      '#c34343': color_secondary,
      '#a03333': color_secondary_darken_40,
      '#8d2d2d': color_secondary_darken_40,
      '#852a2a': color_secondary_darken_40,

      '#5bc0de': color_primary,
      '#3db5d8': color_primary_darken_10,
      '\"color_primary\"': color_primary,
      '\"color_primary_lighten_15\"': color_primary_lighten_15,
      '\"color_primary_darken_10\"': color_primary_darken_10
    };

    var re = new RegExp(Object.keys(mapObj).join("|"), "gi");
    data = data.replace(re, function(matched) {
      return mapObj[matched.toLowerCase()];
    });

    var css = data;
    var head = document.head || document.getElementsByTagName('head')[0];
    var style = document.createElement('style');

    style.type = 'text/css';
    if (style.styleSheet) {
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }

    head.appendChild(style);
    $('#main-css').remove();
    $('body').addClass('css-loaded');

  });

  function ColorLuminance(hex, lum) {
    // validate hex string
    hex = String(hex).replace(/[^0-9a-f]/gi, '');
    if (hex.length < 6) {
      hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }
    lum = lum || 0;
    // convert to decimal and change luminosity
    var rgb = "#",
      c, i;
    for (i = 0; i < 3; i++) {
      c = parseInt(hex.substr(i * 2, 2), 16);
      c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
      rgb += ("00" + c).substr(c.length);
    }
    return rgb;
  }

}

$(document).ready(function() {

  var swiperSideNav = null;
  var screen_size;

  function initSwiperSideNav(direction) {
    if (direction == 'horizontal') {
      var initialSlide = $('.sidemenu-item.active').parent('.swiper-slide').index();
      console.log(initialSlide);
      swiperSideNav = new Swiper('.swiper-side-nav', {
        direction: direction,
        slidesPerView: 'auto',
        initialSlide: initialSlide
      });
    } else {
      if (swiperSideNav != null) {
        swiperSideNav.setWrapperTranslate(0, 0, 0);
        swiperSideNav.destroy();
      }
    }
  }

  $(window).resize(function() {
    var screen_size_change;
    if ($('#test-screen-size').is(":visible")) {
      screen_size_change = 'horizontal';
    } else {
      screen_size_change = 'vertical';
    }
    if (screen_size != screen_size_change) {
      screen_size = screen_size_change;
      initSwiperSideNav(screen_size);
    }
  });

  $(window).trigger('resize');

  $('.checkbox-toggle').bootstrapToggle({
    on: 'Yes',
    off: 'No',
    onstyle: 'success',
    offstyle: 'outline'
  });

  $('input[type="date"]').datepicker({
    format: 'yyyy-mm-dd'
  });

});