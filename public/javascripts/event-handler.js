$(document).ready(function() {

  // SIGNUP
  $('#signup-btn').click(function() {
    // validate form
    validateSignup(function(err) {

      if (err) {
        return;
      }

      // no errors
      // submit
      else {
        // check for errors (user exists)
        $.ajax({
          type: "POST",
          url: '/signup',
          contentType: "application/x-www-form-urlencoded; charset=UTF-8",
          data: $('#signup-form').serialize(),

          // SEND TITLES IN RES
          success: function(res) {
            console.log("success");
            location.reload();

          },
          error: function(xmlhttp, status, error) {

            // useremail exists
            var error_msg = xmlhttp.responseText;
            $('.error').text(error_msg);
          }
        })
      }
    });
  });



  $(document.body).on('click', '.change-lang', function() {

    var lang = $(this).attr('id');
    console.log("translating to " + lang);

    $.ajax({
      type: "POST",
      url: '/translate/' + lang,
      // SEND TITLES IN RES
      success: function(res) {


        console.log("translated");
        // toggle button

        location.reload();

      },
      error: function(xmlhttp, status, error) {
        console.log("error " + error);


      }
    })
  })


  // STREAM LAYOUT EVENTS

  // event for when a logged in admin clicks on a stream thumb
  $(".admin-controllable-thumb").click(function() {

    /* // reset all other controls
     $('.stream-description').show();
     $('.controls').hide();  */
    // show this streams controls
    $(this).find('.stream-description').toggle();
    $(this).find('.controls').toggle();

  });

  $(".edit").click(function() {

    var stream_id = this.id.split('-')[1];
    var form = $(this).parents('form:first');
    var tag = "/edit_" + type + "/" + stream_id;
    form.attr('action', tag);
    form.attr('method', 'get');
    form.submit();

  });

  $(".remove").click(function() {
    var confirmation = confirm("Remove stream?");
    if (confirmation == true) {
      var stream_id = this.id.split('-')[1];
      var form = $(this).parents('form:first');
      form.attr('action', '/remove_stream/' + stream_id);
      form.attr('method', 'post');
      form.submit();
    }
  });


  $(".advanced-settings-btn").click(function() {
    $(".advanced-settings").toggle();
    var text = $(".advanced-settings-btn").text();
    console.log(text);
    if (text == "Show Advanced Settings") {
      $(".advanced-settings-btn").text("Hide Advanced Settings");
    } else {
      $(".advanced-settings-btn").text("Show Advanced Settings");
    }


  })

  // GO SERVICES

  //   $(document.body).on({
  //   mouseenter: function() {
  //      $(this).find('.goservice-remove').show();
  //   },
  //   mouseleave: function() {
  //      $(this).find('.goservice-remove').hide();
  //   }
  // }, '.goservice-box');

  $(document.body).on('click', '.goservice-remove', function() {
    var confirmation = confirm("Remove GO Service link?");
    if (confirmation == true) {
      $.ajax({
        type: "POST",
        url: '/remove_goservice',
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        data: $(this).closest('form').serialize(),


        // SEND TITLES IN RES
        success: function(res) {


          window.location.reload();



        },
        error: function(xmlhttp, status, error) {
          console.log("error " + error);

        }
      });
    }
  });

  // SETTINGS FORM

  var new_langs = '';
  $('#upload-logo').hide();
  $('#new-lang-container').children().hide();

  $('#change-logo').click(function() {
    $('#upload-logo').show();
  })

  // add language
  $('#add-lang').click(function() {
    // $(this).hide();
    $('#new-lang-container').children().show();

  })


  $('#confirm-lang').click(function() {
    $('#new-lang-container').children().hide();
    // $('#add-lang').show();
    var new_lang = $('#new-lang').val();
    console.log("new_lang is " + new_lang);
    // add it to radio buttons
    //  NEW LANG CONTROL ** MAP TO A TAG  I.E ENGLISH -> EN  ** TODO
    var element = '<label class="radio-inline"><input type="radio" name="lang" value="' + new_lang + '"/>' + new_lang +
      '</label>';

    // add homepage input box for this language
    addFieldsForNewLanguage(new_lang);

    // add new lang to hidden input for new langs
    var new_langs = $('#new-langs').val();
    var s = new_lang + " ";
    var new_langs = new_langs + s;
    console.log("new langs after concat " + new_langs);

    // add to enabled langs hidden input ** TODO

    appendToElementListValue(new_lang, '#enabled-langs');

    $('#new-langs').val(new_langs);

    $('#langs-container').append(element);

    $('#added-lang').val('true');

  });

  // show account-related fields on account type select
  $('input:radio[name="payment"]').change(
    function() {
      if ($(this).is(':checked') && $(this).val() == 'stripe') {
        $('#stripe-details').removeClass('hidden');
      }
      // hide stripe related fields if stripe not selected
      else {
        $('#stripe-details').addClass('hidden');
      }
    });

  // dynamic elements

  // sidebar

  $(document.body).on({
    mouseenter: function() {
      $(this).find('.category-control').show();
    },
    mouseleave: function() {
      $(this).find('.category-control').hide();
    }
  }, '.category');


  $(document.body).on('click', '.category-control', function() {
    var category_to_remove = $(this).attr('id').split('-')[2];
    var category_name = $(this).data("category-name");
    var stream_type = $(this).data("type");


    var confirmation = confirm("Remove category " + category_name + "?");
    if (confirmation == true) {
      $.ajax({
        type: "POST",
        url: '/remove_category/' + stream_type + '/' + category_to_remove,
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        data: $(this).closest('form').serialize(),

        // SEND TITLES IN RES
        success: function(res) {

          var redirect_link = 'http://localhost:8094/stream_by_type/' + stream_type;
          window.location.replace(redirect_link);

        },
        error: function(xmlhttp, status, error) {
          console.log("error " + error);

        }
      });
    };
  });
});

function streamSubmitHandler(form_id, avail_categories_list, added_categories, enabled_languages) {
  var categories_input = $("input[name=categories]").val().trim();
  // ad, livestream or vod
  var type = form_id.split('-')[1];
  console.log("the type " + type);

  handleNewCategories(avail_categories_list, added_categories, enabled_languages, function(cancelled) {
    // do form validation as normal
    if (!cancelled) {
      validateForm(type, function(errors) {
        if (!errors) {
          confirmNoCategories(categories_input, function(confirmed) {
              if (!confirmed) {
                // cancel
                // do not submit form
                return;
              }
            })
            // confirm no categories
            // if return false, do not submit
          var form_tag = '#' + form_id;
          console.log("the form tag " + form_tag);
          $(form_tag).submit();
        }
      });
    }
  });
}


//*** HELPERS ***

// append item_to_append to element_tag value
function appendToElementListValue(item_to_append, element_tag) {

  var input_value = $(element_tag).val();
  input_value_list = input_value.split(',');
  input_value_list.push(item_to_append);
  $(element_tag).val(input_value_list);

}