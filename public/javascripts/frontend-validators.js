// FORM VALIDATATORS

// handle categories that DNE
// return: false submits add stream form after closing alert and/or modal, true does not
function handleNewCategories(available_categories,added_categories,enabled_languages,callback){
  var categories_input = $("input[name='categories']").val();
  var categories_list = categories_input.split(",");

  if(categories_input == ""){
    // return to form to submit
    callback(false);
   return;
   }

  checkForUnavailableCategories(available_categories,added_categories,function(new_categories_present,unavailable_categories){
    if(new_categories_present){

      var r = confirm("Add new categories " + unavailable_categories + "?");
      if (r==true){
        addCategories(unavailable_categories,enabled_languages,function(added){

          added_categories = added;
           available_categories.push.apply(available_categories,added_categories);
           instantiateCategoriesTokenfield(available_categories);
           // return back to add stream form without submitting
           callback(true);
        });
      }
      // cancelled
      else{
        callback(true);
      }
    }
    else{
      callback(false);
    }
  });
}


function confirmNoCategories(categories,callback){

   if(validator.isEmpty(categories)){
              var r = confirm("Add stream without categories?");

               if (r==false){
                callback(false)
                }
              else{
                callback(true);
              }
       }
}



// added categories are the categories that were previously dynamically added from this form
function checkForUnavailableCategories(available_categories,added_categories,callback){



   var categories_input = $("input[name='categories']").val();
   var categories_list = categories_input.trim().split(",");
   var unavailable_categories = [];



      for (var i = 0 ; i < categories_list.length ; i ++){

          var category = categories_list[i].trim();

          if (available_categories.indexOf(category) < 0 && added_categories.indexOf(category) < 0  ){
            console.log("category  "  + category + " dne");
            unavailable_categories.push(category.trim());
            }

        }
      // if there are unavailable categories
      if (unavailable_categories.length > 0 ){
        console.log("lenght" + unavailable_categories.length);
         console.log("lenght" + unavailable_categories);



        callback(true,unavailable_categories);
      }
      // no new categories to add
      else{
        console.log("none");
        callback(false,unavailable_categories);
      }
}


function validateSignup(callback){
  var error_count = 0;

   // clear error alerts
  $('#signup .error').empty();


  var email = $("#signup input[name=email]").val();
  var password = $("#signup input[name=password]").val();
  var retype_password = $("#signup input[name=retype-password]").val();

  console.log("the fucking email " + email);


  if (!validator.equals(password,retype_password)){
    error_count ++;
    $('#signup .error').text('Passwords do not match');

    }

    if (!validator.isLength(password,{min:5,max:undefined})){
    error_count ++;
    $('#signup .error').text('Password too short. Min 5 chars');

    }


  if (validator.isAlpha(password)){
    error_count ++;
    $('#signup .error').text('Password must contain at least one number');

    }


  if (!validator.isEmail(email)){
    error_count ++;
    $('#signup .error').text('Provide a valid email');

    }


  if (validator.isEmpty(email) || validator.isEmpty(password)
  || validator.isEmpty(retype_password) ){
    error_count ++;
    $('#signup .error').text('Fill all fields');
  }


  // return true if there are errors
  return callback(error_count > 0);

}

function validateVODForm(callback){

  var error_count=0;

  // clear error alerts
  $('.error').empty();
  $('.has-error').removeClass('has-error');


  var title_input = $("#default-vod-title").val();
  if (validator.isEmpty(title_input)){
    console.log("Provide title");
    //$('#general-error').text('Fill all translation fields');
    error_count ++;
    //$('#title-error').text('Provide a stream title');
    $('#title-error').closest('.form-group').addClass('has-error');
  }

  // ensure video was uploaded

  if (validator.isEmpty($('#video-path').val())){
    //$('#video-error').text('Upload a video');
    $('#video-error').closest('.form-group').addClass('has-error');
    error_count ++;

    }



  if(error_count > 0){
    // do not submit form
    console.log("not submitting");


    // open default lang form section
     $('.default-lang-btn').trigger("click");



    callback(true);
  }

  else{
    callback(false);
  }



}


function validateLivestreamForm(callback){

  var error_count=0;

  // clear error alerts
  $('.error').empty();
  $('.has-error').removeClass('has-error');

  var title_input = $("#default-livestream-title").val();
  if (validator.isEmpty(title_input)){
    console.log("Provide title");
    error_count ++;
    //$('#title-error').text('Provide a stream title');
    $('#title-error').closest('.form-group').addClass('has-error');
    $('#general-error').text('Fill all translation fields');
  }

  var hostname_input = $("input[name=hostname]").val();
  if (validator.isEmpty(hostname_input)){
    console.log("Provide hostname");
    error_count ++;
    //$('#hostname-error').text('Provide a hostname');
    $('#hostname-error').closest('.form-group').addClass('has-error');
  }

  var port_input = $("input[name=port]").val();
  if (validator.isEmpty(port_input) || !(validator.isNumeric(port_input))){
    console.log("Provide hostname");
    error_count ++;
    //$('#port-error').text('Provide a valid port number');
    $('#port-error').closest('.form-group').addClass('has-error');
  }

  if(error_count > 0){
    // do not submit form
    console.log("not submitting");
    $('.default-lang-btn').trigger("click");

    callback(true);
  }

  else{
    callback(false);
  }

}



function validateAdForm(callback){
   var error_count=0;

  // clear error alerts
  $('.error').empty();
  $('.has-error').removeClass('has-error');


  var title_input = $("#default-ad-title").val();
  if (validator.isEmpty(title_input)){
    console.log("Provide title");
    error_count ++;
    //$('#title-error').text('Provide a stream title');
    $('#title-error').closest('.form-group').addClass('has-error');
    $('#general-error').text('Fill all translation fields');
  }

  // ensure video was uploaded

  if (validator.isEmpty($('#video-path').val())){
    //$('#video-error').text('Upload a video');
    $('#video-error').closest('.form-group').addClass('has-error');
    error_count ++;


    }

 if(error_count > 0){
    // do not submit form
    console.log("not submitting");
     $('.default-lang-btn').trigger("click");

    callback(true);
  }

  else{
    callback(false);
  }


}


// dispatcher
function validateForm(type,callback){
  console.log(type);

  if (type == 'ad'){
    validateAdForm(function(has_errors){
      return callback(has_errors);
    })
  }

  else if (type == 'livestream'){
    validateLivestreamForm(function(has_errors){
      return callback(has_errors);
    })
  }

  else if (type == 'vod'){
     validateVODForm(function(has_errors){
      return callback(has_errors);
    })
  }


}



