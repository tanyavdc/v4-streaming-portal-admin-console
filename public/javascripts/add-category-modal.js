var current_language;
var enabled_languages;
var type;
var in_form;
var add_stream_form;

function getNewCategoryLabels(current_language, callback) {

  var added_categories = [];
  var count = $("#count").val();
  console.log("the count " + count);

  for (var i = 0; i < count; i++) {
    var tag = "#" + (i + 1) + "-label-" + current_language;
    var new_category = $(tag).val();
    added_categories.push(new_category);
  }

  if (i == count) {
    if (typeof callback == 'function') {
      callback(added_categories);
      console.log("the added categories are " + added_categories);
    }
  }

}

function addCategories(unavailable_categories, langs, callback) {

  add_stream_form = callback;

  // launch modal prepopulated with unavailable categories
  // return added categories in default lang
  // set to new categories

  // prepopulate modal with categories to add

  // add first category **
  var enabled_languages = langs;
  var language_list = enabled_languages.split(',');
  for (var j = 0; j < language_list.length; j++) {

    var tag = '#1-label-' + language_list[j];
    $(tag).attr('value', unavailable_categories[0]);
  }

  // add extra categories
  for (var i = 1; i < unavailable_categories.length; i++) {

    var category_index = parseInt($('#count').val()) + 1;

    for (var j = 0; j < language_list.length; j++) {

      var element = $($('#clone-extra-category-row').html().trim());
      element.addClass('extra-category-' + category_index);
      element.find('input')
        .attr('name', category_index + '-label-' + language_list[j])
        .attr('id', category_index + '-label-' + language_list[j])
        .attr('value', unavailable_categories[i]);
      element.find('button')
        .attr('id', category_index + '-remove');

      var language_field_tag = "#extra-categories-" + language_list[j];
      $(language_field_tag).append(element);

    }

    // incriment categories count
    var new_count = category_index;
    $('#count').val(category_index);

  }

  $('#add-category').modal('show');
  // launch modal **
  // do not submit form
  // else, go back to form

}

function clearThisForm() {

  $('#add-categories-form').trigger("reset");
  $('.extra-categories').empty();

}

function instantiateCategoryModal(stream_type, context, current_lang, enabled_langs) {

  type = stream_type;
  current_language = current_lang;
  enabled_languages = enabled_langs;
  in_form = context;

  $('.categories-lang-field').hide();
  var label_tag = "#categories-" + current_language;
  $(label_tag).show();

  // hide fields that are not current lang fields

}


$(document).ready(function() {

  $('#add-category').on('hidden.bs.modal', function() {
    clearThisForm();
  })

  // remove a category field
  $(document.body).on('click', '.remove-category-btn', function() {

    var category_index = $(this).attr('id').split("-")[0];

    var category_field_tag = ".extra-category-" + category_index;
    $(category_field_tag).remove();

    // decriment count of categories to add
    var category_index = parseInt($('#count').val()) - 1;

    $('#count').val(category_index);

  })

  // hide fields that are not current lang fields

  // MOVE TO LANGUAGE FORM CONTROL ** TODO

  // SHOW current LANG FIELDS **
  $(".categories-lang-btn").click(function() {

    // show fields of lang field
    var tag = this.id.split('-');
    var lang = tag[1];
    var label_tag = "#categories-" + lang;

    // hide non currnt lang fields if not on currentlang
    if (lang != current_language) {
      $('.categories-non-lang').hide();
    } else {
      $('.categories-non-lang').show();
    }

    // hide all lang fields that are not of chosen lang
    $('.categories-lang-field').hide();
    $('.categories-lang-btn').removeClass('active');
    $(this).addClass('active');

    $(label_tag).show();

  });

  $('.add-category-btn').click(function() {

    // add a new input field on all languages

    // for lang in enable languages

    // append to categories-[lang]
    // appending <row> <col-xs-10><input tpe="text" name="[count]-label-[lang]"

    var language_list = enabled_languages.split(',');
    console.log(language_list);

    var category_index = parseInt($('#count').val()) + 1;
    console.log(category_index);
    for (var i = 0; i < language_list.length; i++) {
      console.log("count");

      var element = $($('#clone-extra-category-row').html().trim());
      element.addClass('extra-category-' + category_index);
      element.find('input')
        .attr('name', category_index + '-label-' + language_list[i])
        .attr('id', category_index + '-label-' + language_list[i]);
      element.find('button')
        .attr('id', category_index + '-remove');

      var language_field_tag = "#extra-categories-" + language_list[i];
      $(language_field_tag).append(element);

    }

    // incriment categories count
    var new_count = category_index;
    $('#count').val(category_index);

  });

  // category submit
  $('#categories-submit-btn').click(function() {

    console.log("clicked");

    var error_count = 0;

    // clear error alerts
    $('.error').empty();

    // validate

    var elements = document.getElementById("add-categories-form").elements;
    var elements_by_lang = [];

    for (var i = 0, element; element = elements[i++];) {
      if (element.type === "text" && hasClass(element, "label-translations")) {
        console.log("element val " + element.value);
        console.log("element name " + element.name);
        var lang = element.name.split('-')[2];
        elements_by_lang.push(element.value + '-' + lang);

        // check if fields are filled
        if (validator.isEmpty(element.value)) {

          $('#label-error').text('Fill all fields and required translations');
          error_count++;

        }

        // check if category already exists

        // LANG CONTROL : NEED TO CHECK WITH AVAIL CATEGORIES OF OTHER LANGS TOO
        // CREATE A MAP IN CACHE SETUP
        // OR BACKEND VALIDATION FOR UNIQUE LABEL CONTRAINT VIOLATION

      }

    }

    // ensure no duplicate categories
    console.log(elements_by_lang);
    if (hasDuplicates(elements_by_lang)) {
      $('#label-error').text('Duplicate category names');
      error_count++;
    }

    if (error_count > 0) {
      // do not submit form
      console.log("not submitting");
    }

    // no errors
    else {

      if (in_form) {

        // get the categories to add to send back to form

        getNewCategoryLabels(current_language, function(new_categories) {

          console.log("THE NEW CATEGORIRES  " + new_categories);

          $.ajax({
            type: "POST",
            url: '/add_categories/' + type,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            data: $("#add-categories-form").serialize(),

            // SEND TITLES IN RES
            success: function(res) {

              alert("Successful new categories add")
              $('#add-category').modal('toggle');
              add_stream_form(new_categories);
              // go back to default lang section in form
              $('.default-lang-btn').trigger("click");

            },
            error: function(xmlhttp, status, error) {
              console.log("error " + error);

              $('#label-error').text('Category exists');
            }
          });

        });

      } else {

        $.ajax({
          type: "POST",
          url: '/add_categories/' + type,
          contentType: "application/x-www-form-urlencoded; charset=UTF-8",
          data: $("#add-categories-form").serialize(),

          // SEND TITLES IN RES
          success: function(res) {

            alert("Successful new categories add")
            $('#add-category').modal('toggle');
            window.location.reload();


          },
          error: function(xmlhttp, status, error) {
            console.log("error " + error);

            $('#label-error').text('Category exists');
          }
        });

      }

    }
  });

  $(document.body).on('input', '.label-translations', function() {

    // get the category number
    var category_number = $(this).attr('name').split('-')[0];

    // set a placeholder in this lang for all other lang fields for this category number
    var l = enabled_languages
    var langs = l.split(',');
    for (var i = 0; i < langs.length; i++) {
      var category_translation_tag = "#" + category_number + "-label-" + langs[i];
      var placeholder_text = $(this).val();
      $(category_translation_tag).attr("placeholder", placeholder_text);
      // PLACEHOLDER NOT SUPPORTED IN FIREFOX
      //$('#inputTextID').attr("placeholder","placeholder text");
    }

  });

});



//*** HELPERS *****

function hasDuplicates(array) {
  var valuesSoFar = Object.create(null);
  for (var i = 0; i < array.length; ++i) {
    var value = array[i];
    if (value in valuesSoFar) {
      return true;
    }
    valuesSoFar[value] = true;
  }
  return false;
}

function hasClass(element, cls) {
  return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}