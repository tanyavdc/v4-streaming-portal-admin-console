$(document).ready(function(){

Dropzone.options.livestreamThumb = {
      paramName:"livestreamThumb",
      maxFilesize: 1024, // MB
      url: "/upload_livethumb",
      addRemoveLinks: true,
      acceptedFiles: ".jpeg,.png,.gif,.jpg",
      removedfile: function(file) {
          var _ref;
          // point thumbnail path back to default
          $("#livestream-thumbpath").val('/tmp/thumbs/default-thumbnail.jpg');
          return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        },
       init: function() {
        this.on("success", function(file, responseText) {

            // send uploaded thumbnail filepath to main form 
            //  $('#thumb').val(responseText.path);  THIS PATH INCLUDES 'public/' at beginning 
            var raw_path = responseText.path;
            // remove "public/" from path
            var path = raw_path.replace("public/","/");

            $("#livestream-thumbpath").val(path);
 
        });
        // overwrite old upload if new file is dropped in 
         this.on("addedfile", function() {
          if (this.files[1]!=null){
            this.removeFile(this.files[0]);
          }
    });
        }
      };

 Dropzone.options.vodThumb = {
paramName:"vodThumb",
maxFilesize: 1024, // MB
url: "/upload_vodthumb",
addRemoveLinks: true,
acceptedFiles: ".jpeg,.png,.gif,.jpg",
removedfile: function(file) {
    var _ref;
    // point thumbnail path back to default
    $("#ondemand-thumbpath").val('/tmp/thumbs/default-thumbnail.jpg');
    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
  },
 init: function() {
  this.on("success", function(file, responseText) {

      // send uploaded thumbnail filepath to main form 
      //  $('#thumb').val(responseText.path);  THIS PATH INCLUDES 'public/' at beginning 
      var raw_path = responseText.path;
      // remove "public/" from path
      var path = raw_path.replace("public/","/");
      $("#ondemand-thumbpath").val(path);

  });
  // overwrite old upload if new file is dropped in 
   this.on("addedfile", function() {
    if (this.files[1]!=null){
      this.removeFile(this.files[0]);
     }
    });
    }
  }


Dropzone.options.goserviceThumb = {
paramName:"goserviceThumb",
maxFilesize: 1024, // MB
url: "/upload_gothumb",
acceptedFiles: ".jpeg,.png,.gif,.jpg",
addRemoveLinks: true,
removedfile: function(file) {
    var _ref;
    // point thumbnail path back to default
    $("#goservice-thumbpath").val('/tmp/thumbs/default-thumbnail.jpg');
    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
  },
 init: function() {
  this.on("success", function(file, responseText) {

      // send uploaded thumbnail filepath to main form 
      //  $('#thumb').val(responseText.path);  THIS PATH INCLUDES 'public/' at beginning 
      var raw_path = responseText.path;
      // remove "public/" from path
      var path = raw_path.replace("public/","/");
      $("#goservice-thumbpath").val(path);

  });
  // overwrite old upload if new file is dropped in 
   this.on("addedfile", function() {
    if (this.files[1]!=null){
      this.removeFile(this.files[0]);
     }
    });
    }
  }




  Dropzone.options.adThumb = {
      paramName:"adThumb",
      maxFilesize: 1024, // MB
      url: "/upload_adthumb",
      acceptedFiles: ".jpeg,.png,.gif,.jpg",
      addRemoveLinks: true,
      removedfile: function(file) {
          var _ref;
          // point thumbnail path back to default
          $("#ad-thumbpath").val('/tmp/thumbs/default-thumbnail.jpg');
          return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        },
       init: function() {
        this.on("success", function(file, responseText) {

            // send uploaded thumbnail filepath to main form 
            //  $('#thumb').val(responseText.path);  THIS PATH INCLUDES 'public/' at beginning 
            var raw_path = responseText.path;
            // remove "public/" from path
            var path = raw_path.replace("public/","/");
            $("#ad-thumbpath").val(path);
 
        });
        // overwrite old upload if new file is dropped in 
         this.on("addedfile", function() {
          if (this.files[1]!=null){
            this.removeFile(this.files[0]);
           }
          });
          }
        }

Dropzone.options.adVid = {
paramName:"adVid",
maxFilesize: 1024, // MB
url: "/upload_ad",
acceptedFiles:".mp4,.mkv,.avi,.webm,.ogg,.ogv",
addRemoveLinks: true,
removedfile: function(file) {
    var _ref;
    // point thumbnail path back to null
    $("#video-path").val('');
    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
  },
 init: function() {
  this.on("success", function(file, responseText) {

      // send uploaded thumbnail filepath to main form 
      //  $('#thumb').val(responseText.path);  THIS PATH INCLUDES 'public/' at beginning 
      var raw_path = responseText.path;
      // remove "public/" from path
      var path = raw_path.replace("public/","/");

      $("#video-path").val(path);

      // clear no video uploaded error 
      $('#video-error').empty();
     

  });
  // overwrite old upload if new file is dropped in 
   this.on("addedfile", function() {
    if (this.files[1]!=null){
      this.removeFile(this.files[0]);
     }
    });
    }
  }


Dropzone.options.vodVid = {
      paramName:"vodVid",
      maxFilesize: 1024, // MB
      url: "/upload_vod",
      addRemoveLinks: true,
      acceptedFiles:".mp4,.mkv,.avi,.webm,.ogg,.ogv",
      removedfile: function(file) {
          var _ref;
          // point thumbnail path back to null
          $("#video-path").val('');
          return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        },
       init: function() {
        this.on("success", function(file, responseText) {

            // send uploaded thumbnail filepath to main form 
            //  $('#thumb').val(responseText.path);  THIS PATH INCLUDES 'public/' at beginning 
            var raw_path = responseText.path;
            // remove "public/" from path

            var path = raw_path.replace("public/","/");
            $("#video-path").val(path);


            // clear no video uploaded error 
            $('#video-error').empty();
           
 
        });
        // overwrite old upload if new file is dropped in 
         this.on("addedfile", function() {
          if (this.files[1]!=null){
            this.removeFile(this.files[0]);
           }
          });
          }
        }


});