 var title_tag;
 var description_tag;
 var homepage_msg_tag;
 var current_lang;
 var enabled_langs;



 function instantiateLanguageFields(current_language, enabled_languages) {
   $('.lang-field').hide();

   current_lang = current_language;
   enabled_langs = enabled_languages;

   title_tag = "#title-" + current_lang;
   description_tag = "#description-" + current_lang;
   // for settings
   homepage_msg_tag = "#homepage-msg-" + current_lang;


   $(title_tag).show();
   $(description_tag).show();
   $(homepage_msg_tag).show();



 }

 $(document).ready(function() {


   $(document.body).on('click', '.lang-btn', function() {


     // show fields of lang field
     var tag = this.id.split('-');
     var lang = tag[1];
     var title_tag = "#title-" + lang;
     var description_tag = "#description-" + lang;
     // for settings form
     var homepage_msg_tag = "#homepage-msg-" + lang;

     // hide non currnt lang fields if not on currentlang
     if (lang != current_lang) {
       $('.non-lang').hide();
     } else {
       $('.non-lang').show();
     }

     // hide all lang fields that are not of chosen lang
     $('.lang-field').hide();
     $(title_tag).show();
     $(description_tag).show();

     $(homepage_msg_tag).show();



     $('.lang-btn').removeClass('active');
     $(this).addClass('active');
   });

   // category modal


 })


 // used in settings form when a new language is added
 function addFieldsForNewLanguage(new_language) {

   console.log("the new language in addfieldsfornewlang " + new_language);
   // add button to lang control
   var new_lang_button = $($('#clone-lang-btn').html().trim());
   new_lang_button
    .attr('id','settings-' + new_language)
    .html(new_language);
   $('.lang-toggles').append(new_lang_button)

   // add homepage msg input box
   var homepage_msg_trans = $($('#clone-homepage-msg-container').html().trim());
   homepage_msg_trans.hide()
    .attr('id','homepage-msg-' + new_language)
    .find('input')
      .attr('name','homepage-msg-' + new_language);
   $('#homepage-msg-container').append(homepage_msg_trans);

 }