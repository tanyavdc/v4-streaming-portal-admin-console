$(document).ready(function(){


       Dropzone.options.livestreamThumb = {
      paramName:"livestreamThumb",
      maxFilesize: 1024, // MB
      url: "/upload_livethumb",
      addRemoveLinks: true,
      clickable: '.thumbnail',
      acceptedFiles: ".jpeg,.png,.gif,.jpg",
      removedfile: function(file) {
          var _ref;
          // point thumbnail path back to default
          $("#livestream-thumbpath").val('/tmp/thumbs/default-thumbnail.jpeg');
          $("#current-thumbnail-container").show();
          $("#new-thumbnail-container").hide();
          return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;

        },
       init: function() {
        this.on("success", function(file, responseText) {

         $("#current-thumbnail-container").hide();
            $("#new-thumbnail-container").show();

            // send uploaded thumbnail filepath to main form 
            //  $('#thumb').val(responseText.path);  THIS PATH INCLUDES 'public/' at beginning 
            var raw_path = responseText.path;
            // remove "public/" from path
            var path = raw_path.replace("public/","/");

            $("#livestream-thumbpath").val(path);
 
        });
        // overwrite old upload if new file is dropped in 
         this.on("addedfile", function() {
          if (this.files[1]!=null){
            this.removeFile(this.files[0]);
          }
        });
        }
      };

 Dropzone.options.vodThumb = {
      paramName:"vodThumb",
      maxFilesize: 1024, // MB
      url: "/upload_vodthumb",
      acceptedFiles: ".jpeg,.png,.gif,.jpg",
      addRemoveLinks: true,
      clickable: '.thumbnail',
      removedfile: function(file) {
          var _ref;
         
          $("#current-thumbnail-container").show();
          $("#new-thumbnail-container").hide();
           // point thumbnail path back to default
          $("#vod-thumbpath").val('/tmp/thumbs/default-thumbnail.jpeg');
          return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        },
       init: function() {
        this.on("success", function(file, responseText) {

            $("#current-thumbnail-container").hide();
            $("#new-thumbnail-container").show();

            // send uploaded thumbnail filepath to main form 
            //  $('#thumb').val(responseText.path);  THIS PATH INCLUDES 'public/' at beginning 
            var raw_path = responseText.path;
            // remove "public/" from path
            var path = raw_path.replace("public/","/");

            $("#vod-thumbpath").val(path);
 
        });
        // overwrite old upload if new file is dropped in 
         this.on("addedfile", function() {
          if (this.files[1]!=null){
            this.removeFile(this.files[0]);
          }
        });
        }
      };


      Dropzone.options.vodVid = {
      paramName:"vodVid",
      maxFilesize: 1024, // MB
      url: "/upload_vod",
      clickable: '.upload-video-btn',
      acceptedFiles:".mp4,.mkv,.avi,.webm,.ogg,.ogv",
      
       init: function() {
        this.on("success", function(file, responseText) {


            $("#current-video-container").hide();
            $("#new-video-container").show();
            $("#video-playback")[0].pause();

            // send uploaded thumbnail filepath to main form 
            //  $('#thumb').val(responseText.path);  THIS PATH INCLUDES 'public/' at beginning 
            var raw_path = responseText.path;
            // remove "public/" from path
            var path = raw_path.replace("public/","/");
            console.log("the path " + path);
            $("#video-path").val(path);
 
        });
        // overwrite old upload if new file is dropped in 
         this.on("addedfile", function() {
          if (this.files[1]!=null){
            this.removeFile(this.files[0]);
           }
          });
          }
        }

      
 Dropzone.options.adThumb = {
      paramName:"adThumb",
      maxFilesize: 1024, // MB
      url: "/upload_adthumb",
      addRemoveLinks: true,
      clickable: '.thumbnail',
      acceptedFiles: ".jpeg,.png,.gif,.jpg",
      removedfile: function(file) {
          var _ref;
         
          $("#current-thumbnail-container").show();
          $("#new-thumbnail-container").hide();
           // point thumbnail path back to default
          $("#ad-thumbpath").val('/tmp/thumbs/default-thumbnail.jpeg');
          return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        },
       init: function() {
        this.on("success", function(file, responseText) {

            $("#current-thumbnail-container").hide();
            $("#new-thumbnail-container").show();

            // send uploaded thumbnail filepath to main form 
            //  $('#thumb').val(responseText.path);  THIS PATH INCLUDES 'public/' at beginning 
            var raw_path = responseText.path;
            // remove "public/" from path
            var path = raw_path.replace("public/","/");

            $("#ad-thumbpath").val(path);
 
        });
        // overwrite old upload if new file is dropped in 
         this.on("addedfile", function() {
          if (this.files[1]!=null){
            this.removeFile(this.files[0]);
          }
        });
        }
      };


      Dropzone.options.adVid = {
      paramName:"adVid",
      acceptedFiles:".mp4,.mkv,.avi,.webm,.ogg,.ogv",
      maxFilesize: 1024, // MB
      url: "/upload_ad",
      clickable: '.upload-video-btn',
       init: function() {
        this.on("success", function(file, responseText) {


            $("#current-video-container").hide();
            $("#new-video-container").show();
            $("#video-playback")[0].pause();

            // send uploaded thumbnail filepath to main form 
            //  $('#thumb').val(responseText.path);  THIS PATH INCLUDES 'public/' at beginning 
            var raw_path = responseText.path;
            // remove "public/" from path
            var path = raw_path.replace("public/","/");
            $("#video-path").val(path);
 
        });
        // overwrite old upload if new file is dropped in 
         this.on("addedfile", function() {
          if (this.files[1]!=null){
            this.removeFile(this.files[0]);
           }
          });
          }
        }

});