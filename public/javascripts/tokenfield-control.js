// tokenfield
function instantiateCategoriesTokenfield(avail_categories_list){
  console.log("the avail cats in tokenfield control " + avail_categories_list);
  $('.categories-input').tokenfield('destroy');

   $('.categories-input').tokenfield({
      autocomplete: {
        source: avail_categories_list,
        delay: 100
      },
      showAutocompleteOnFocus: true,
      beautify: false,
      createTokensOnBlur:true
      });

      $('.categories-input').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
      });


    });

    
  

}


function instantiateAdsTokenfield(avail_ads_list){

  


    $('.ads-input').tokenfield({
      autocomplete: {
        source: avail_ads_list,
        delay: 100
      },
      showAutocompleteOnFocus: true,
      beautify: false,
      createTokensOnBlur:true,
      limit:1
      });

      $('.ads-input').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
      });


    });
      
  

}

function instantiateTagsTokenfield(){

  


    $('.tags-input').tokenfield({
      showAutocompleteOnFocus: true,
      beautify: false,
      createTokensOnBlur:true
      });

      $('.tags-input').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function(index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
      });


    });
      
  

}
