// var type;
// var current_language;
// var category;

// function instantiateEditCategoryModal(stream_type,category_label,current_lang,){ 

//     type = stream_type; 
//     current_language= current_lang;
//     category= category_label;

//   }


$( document ).ready(function(){ 


 $('#edit-category-submit').click(function(){

    console.log("clicked");

    var error_count=0; 

      // clear error alerts 
      $('.error').empty(); 

      var id = $("input[name='id']").val(); 

      var new_label = $("input[name='new-label']").val(); 

      var language = $("input[name='language']").val(); 

      var type = $("input[name='type']").val(); 

      console.log("the lang " + language); 
      console.log("the type " + type); 


      if (validator.isEmpty(new_label)){
      
      
        $('.error').text('Provide a category name');
        error_count ++; 


      }

      // no errors 
      if(error_count == 0){

          $.ajax({
        type: "POST",
        url: '/edit_category/' + id,
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        data: $("#edit-category-form").serialize(),

        // SEND TITLES IN RES 
        success: function (res) {
          
          alert("Successful category edit")
          $('#edit-category-modal').modal('toggle');

          var redirect_link = 'http://localhost:8094/stream_by_category/' 
          + type + '/' + id;
         window.location.replace(redirect_link);

        
        },
        error: function(xmlhttp, status, error){ 
          console.log("error " + error);
         
        }
      });


      }


    
    });
});