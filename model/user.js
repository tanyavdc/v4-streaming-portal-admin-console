var pg = require('pg');
var db_client = require('./database');
var cache = require('./cache');

/*server declaration
...
...
*/

require('jquery');


exports.signup = function(email,hashed_password,callback){
 var query = db_client.query("INSERT into users (email,password,type) values"+
    " ($1,$2,'admin')",
    [email,hashed_password]);

   query.on("row",function(row,result){
    result.addRow(row);
   })

   query.on("error", function(err){
    // unique violation
    // user exists
    return callback(err);

   })

   query.on("end",function(result){

    return callback(undefined);
  });
}

// CACHE

exports.getSettings = function(callback){

  var data = {};

  // cache hgetALl  ?

  cache.hgetall('domain',function(err,reply){

    console.log("cache reply " + reply);
    console.log("cache reply " + JSON.stringify(reply));

    // set to locals *

    callback(undefined,reply);
  })
}
// NEC ** ??

exports.getSettingsDB = function(callback){

  var homepage_msgs = {};



  var query = db_client.query("SELECT * from settings");


  query.on("row", function (row, settings) {
      settings.addRow(row);

  });

  query.on("end", function (settings) {

     var query = db_client.query("SELECT * from settings2lang");

     query.on("row", function (row) {
      homepage_msgs[row.language] = row.homepage_msg

    })

     query.on("end",function(result){

      return callback(undefined,settings.rows[0],homepage_msgs);


     })
    })

  }




/*
  cache.hgetall('domain',function(err,reply){

    console.log("cache reply " + reply);
    console.log("cache reply " + JSON.stringify(reply));

    // set to locals *

    callback(reply);
  })


} */

// set to app locals things that admin portal needs always
exports.setSettings = function(req,callback){

  var payment_gateway = req.body['payment'];
    var website_label= req.body['website-label'];
    var website_url= req.body['website-url'];
    var facebook_api_token=req.body['facebook-api-token'];
    var facebook_pageID =req.body['facebook-pageID'];

    var primary_colour = req.body['primary-colour'];
    var secondary_colour = req.body['secondary-colour'];

    var payment_gateway = req.body['payment'];
    var stripe_s_key = req.body['stripe-s-key'];
    var stripe_p_key = req.body['stripe-p-key'];

    var enabled_services = req.body['enabled'];

    var home_theme = req.body['home-theme'];

    if(payment_gateway != 'none'){
      enabled_services.push('account');
    }

   cache.hset('domain','payment-gateway', payment_gateway, function(err,reply){
     cache.hset('domain','home-theme', home_theme, function(err,reply){
       cache.hset('domain','website-label', website_label, function(err,reply){
           cache.hset('domain','website-url', website_url, function(err,reply){
               cache.hset('domain','facebook-api-token', facebook_api_token, function(err,reply){
                 cache.hset('domain','facebook-pageID', facebook_pageID, function(err,reply){
                  cache.hset('domain','primary-colour', primary_colour, function(err,reply){
                      req.app.locals.settings['primary_colour'] = primary_colour;
                    cache.hset('domain','secondary-colour', secondary_colour, function(err,reply){
                       req.app.locals.settings['secondary_colour'] = secondary_colour;

                         cache.hset('domain','payment-gateway', payment_gateway, function(err,reply){
                           cache.hset('domain','stripe-s-key', stripe_s_key, function(err,reply){
                            cache.hset('domain','stripe-p-key', stripe_p_key, function(err,reply){

                              listToCache('enabled-services',enabled_services,function(){
                               setHomepageMsgTranslations(req,function(){
                                callback();
                              });
                            });
                          });
                        });
                      })
                    })
                  });
                });
              });
            });
          });
        });
      });
}


// DB SET SETTINGS
// nec *** ? ? ?
exports.setSettingsDB = function(req,callback){

    var payment_gateway = req.body['payment'];
    var website_label= req.body['website-label'];
    var website_url= req.body['website-url'];
    var facebook_api_token=req.body['facebook-api-token'];
    var facebook_pageID =req.body['facebook-pageID'];
    var default_language = req.body.lang;
    var logo = "";


    var primary_color = req.body['primary-color'];
    var secondary_color = req.body['secondary-color'];

    var languages = req.body['enabled-langs'].split(',');

    console.log("the primary color " + primary_color);
    console.log("the secondary color " + secondary_color);



    // for lang in enabled langs get homepage msg ** TODO
    // loop , similar to getting stream titles?



    // clear settings
    var query = db_client.query("TRUNCATE table settings");


    query.on("end", function () {

       // set settings

      var query = db_client.query("INSERT into settings (website_label,website_url,payment_gateway,logo," +
        "fb_api_token,fb_page_id,primary_colour,secondary_colour,default_language) values ($1,$2,$3,$4,$5,$6,$7,$8,$9)",
         [website_label,website_url,payment_gateway,logo,facebook_api_token,facebook_pageID,
         primary_color,secondary_color,default_language]);

       query.on("end", function () {

          // set enabled languages

          var query = db_client.query("TRUNCATE table enabled_languages");

           query.on("end", function () {

           formatListForDBInsert(languages,function(insert_values){



            var query_string = "INSERT into enabled_languages values " + insert_values;

           console.log("the query string " + query_string);

            var query = db_client.query(query_string);

            query.on("error",function(err){
              console.log("error in set settings enabled langs " + err);
              callback(err);
            })

            query.on("end",function(){

               // set homepage msgs

                var query = db_client.query("TRUNCATE table settings2lang");



                 query.on("end", function () {

                   // set settings
                  // create map language to homepage_msg

                  const insert_values = languages.map(lang => `('${lang}','${req.body["homepage-msg-" + lang]}')`).join(', ');


                  var query_string = "INSERT into settings2lang values " + insert_values;
                  console.log("the query string " + query_string);
                  var query = db_client.query(query_string);

                  query.on("end", function () {
                      console.log("done");
                      callback(undefined);
                    });

                  query.on("error",function(err){
                    callback(err);
                  })
                });
              });
            });
         });
        });
     });
  }



// NEC ???  **
exports.enableServicesDB = function(req,callback){

  var enabled_services = req.body['enabled'];

  console.log("enabled services " + enabled_services);


   var query = db_client.query("TRUNCATE table enabled_services");

       query.on("end", function () {


          formatListForDBInsert(enabled_services,function(insert_values){
            var query_string="INSERT into enabled_services values " + insert_values;
            console.log("query string " + query_string)
            var query= db_client.query(query_string)
          query.on("error",function(err){
            console.log("error in eabled serices " + err);

            callback(err);
           })

          query.on("end",function(){
            callback(undefined);

        });
       });
      });
    }

exports.addNewLanguages = function(new_langs,enabled_languages,callback){
   // NEEDS ASYNC CONTROL **
    for(var i=0;i<new_langs.length;i++){
      console.log("loopin");
        enabled_languages.push(new_langs[i]);
        cache.rpush('enabled-languages',new_langs[i]);

        };

   if(i == new_langs.length){
     if(typeof callback=='function'){

              callback();
              }
   }
}

// return the categories added as list
exports.setAndGetCachedCategories = function(langs,livestream_categories,ondemand_categories,ad_categories,callback){

    console.log("the tag " + livestream_categories);
    console.log("the tag " + ondemand_categories);
    cache.lrange(livestream_categories,0,-1,function(err,livestream_categories_results){
        // copy default lang categories to new lang categories list

         cacheCategories('livestream',langs,livestream_categories_results,function(){


           cache.lrange(ondemand_categories,0,-1,function(err,ondemand_categories_results){

            cacheCategories('ondemand',langs,ondemand_categories_results,function(){



            cache.lrange(ad_categories,0,-1,function(err,ad_categories_results){
              console.log("done ad");
            // copy default lang categories to new lang categories list

              cacheCategories('advertisement',langs,ad_categories_results,function(){
                  callback(livestream_categories_results,ondemand_categories_results,ad_categories_results);

              // add these categories to this language in db
                  });
                });
              });
            });
          });
        });
  }




exports.addCategoryTranslations=function(default_lang,new_langs,
  type,categories,callback){
  var done = 0;
  // for each language
  for(var j = 0 ; j < new_langs.length ; j ++){
    var new_lang = new_langs[j];

    // for each category label in default language
    for(var i = 0 ; i < categories.length ; i ++){
      var category = categories[i];
      // get the category id
      console.log("the category in addcateogrytranslations main " + category)
      getCategoryIDByLabel(category,default_lang,type,function(id,default_label){
        addCategoryTranslation(id,new_lang,default_label,function(){
          console.log("added");
          done ++;
           if(j == new_langs.length && done==categories.length){
             callback();
           }

        })

      });
    }
  }

  if(j == new_langs.length && done==categories.length){
    callback();
  }
  // for cat in category
  // query to get id
  // in
  // done ++



}


//** HELPERS **
function addCategoryTranslation(category_id,lang,label,callback){
  var query = db_client.query("INSERT into category2lang (id,language,label) "
    + " values ($1,$2,$3)", [category_id,lang,label]);

  query.on("row", function (row, result) {
      result.addRow(row);

    });

    query.on("error", function (err) {
      console.log(err)

    });

    query.on("end", function (result) {
      callback();
    });


}


function getCategoryIDByLabel(label,language,type,callback){

  var query = db_client.query("SELECT c.id,cl.label from category2lang cl JOIN category c on c.id=cl.id WHERE cl.language=$1"
    + " AND cl.label=$2 AND c.type=$3",
        [language,label,type]
         );

  query.on("row", function (row, result) {
      result.addRow(row);

    });

    query.on("error", function (err) {
      console.log(err)

    });

    query.on("end", function (result) {
      callback(result.rows[0].id,result.rows[0].label);
    });


}




function cacheCopy(default_categories,new_lang_categories_tag,callback){
  console.log("Categories in cache copy " + default_categories);
  for(var i = 0; i < default_categories.length; i++){
    cache.rpush(new_lang_categories_tag,default_categories[i]);
  }
  if(i == default_categories.length){
    console.log("done cache copy");
    callback();
  }

}

// ie. en,fr,sp   becomes ('en'),('fr'),('sp')
function formatListForDBInsert(lst,callback){

  var insert_values = "";

  for (var i = 0 ; i < lst.length ; i ++ ){

    var s = "('";
    s = s + lst[i] + "')"
    // if not the last item
    if (i < (lst.length-1)){
      s = s + ","
    }
    insert_values = insert_values + s;

  }

    if(i == lst.length){
    console.log("formatted insert values " + insert_values);
    callback(insert_values);
  }


}
 // sets a list of items to cache list with name cachel_lst_tag

 function listToCache(cache_lst_tag,items_to_push,callback){

  // clear existing list in cache_list_tag
  cache.del(cache_lst_tag,function(reply){
     for (var i = 0 ; i < items_to_push.length ; i ++){
    cache.rpush(cache_lst_tag,items_to_push[i])

      }
      if(i == items_to_push.length){

        callback();
      }
  })


 }



function setHomepageMsgTranslations(req,callback){

  var enabled_languages = req.body['enabled-langs'];
  var lang_list = enabled_languages.split(',');

  console.log("sethomepagemsgtranslations lagnsg " + enabled_languages);

  var done = 0;

  for (var i = 0 ; i < lang_list.length ; i ++ ){
      // get the homepage msg for this language
      var tag = 'homepage-msg-' + lang_list[i];
      var homepage_msg_trans = req.body[tag];
      console.log("trans homepage msg " + homepage_msg_trans);

      // add to cache
      cache.hset('domain',tag, homepage_msg_trans, function(err,reply){
          done ++;
          if(done == lang_list.length){
           callback();
          }
        });
    }
 if(i == lang_list.length && done == lang_list.length){
    callback();
 }

}

function cacheCategories(type,new_langs,default_lang_categories,callback){

   var tag = type + "-categories";
   var done=0;


  // for language
  for (var i = 0 ; i < new_langs.length ; i ++){
    var new_lang_categories_tag= type + '-categories-' + new_langs[i];
    console.log("here in cache categories");


  // check if this language's categories exist in cache
  cache.exists(new_lang_categories_tag,function(err,reply){

    // if dne
    // instantiate with default langs categories
    if(reply == 0){

        cacheCopy(default_lang_categories,new_lang_categories_tag,function(){
          done++;
          if(done==new_langs.length){
            callback();
          }
        });



    }
    // this language already exists in categories cache
    else{
      done ++;
      if(done==new_langs.length){
            callback();
          }
    }
    // else loop to next language
  });
}


if(done==new_langs.length){
  callback();
}



}


