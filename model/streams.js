var pg = require('pg');
var db_client = require('./database');
var cache = require('./cache');
var wowza = require('wowza-securetoken')
var moment = require('moment');

/*server declaration
...
...
*/

require('jquery');


// GETS 

exports.getGoServices=function(callback){

	var query = db_client.query("SELECT id,title,thumbnail,link FROM goservice");

	query.on("row", function (row, result) {
	    result.addRow(row);
		    
		});
			 
		query.on("end", function (result) {

			console.log("results from model " + JSON.stringify(result))
		
	    return callback(undefined,result.rows);
			       
		}); 
		
}

exports.addGoService= function(req,callback){
	var title = req.body.title;
	var link = req.body.link;
	var thumb = req.body.thumb;

	var query = db_client.query("INSERT into goservice (title,link,thumbnail)" +
		" values ($1,$2,$3)", [title,link,thumb]);

			 
		query.on("end", function () {
				 
		 //db_client.end();
	     return callback(undefined);
			       
		}); 

		query.on("error",function(err){
			console.log("error in add goservice " + err);
		})
}

exports.getStreamCountsByType = function(stream_type,language,callback){

	// [{"id":383,"count":"1"}]

	var label_to_count = {};

	// get category id 
  var query = db_client.query(' SELECT c.id,cl.label,count(c.id) as count' +
   ' from stream2category sc join category c on c.id=sc.category_id join category2lang cl' +
   ' on cl.id=c.id where c.type=$1 and cl.language=$2 group by c.id,c.type,cl.label',
   [stream_type,language]); 



 query.on("row", function (row, counts) {
	    label_to_count[row.label] = row.count; 
		    
		});

	query.on("end", function (counts) {

		var query = db_client.query('select count(type) as count from stream' +
	' where type=$1 group by type',[stream_type]); 



			query.on("row", function (row, result) {
			console.log("getstreamcountsbytype " + JSON.stringify(row))
			label_to_count['all'] = row.count
		
	  
	    
		});
		 
		query.on("end", function (result) {

	   	return callback(undefined,label_to_count);
	 	});
			       
	}); 
		
} 

exports.removeGoService = function(id,callback){
	var query = db_client.query("DELETE FROM goservice WHERE id=$1", [id]);

	 
	query.on("end", function () {
			 
	 //db_client.end();
     return callback(undefined);
		       
	}); 
	

}


exports.getAdTitles= function(lang,callback){

	var ids_concat_titles = []; 

	// add new category to database 
	var query = db_client.query("SELECT s.id,sl.title FROM STREAM s" +
		" JOIN STREAM2LANG sl ON sl.id = s.id" +
		" WHERE s.type='advertisement' and sl.language=$1", [lang]);

	query.on("row", function (row) {

			var ad_tag= row.id + ": " + row.title; 

			 ids_concat_titles.push(ad_tag); 
			    
		});
			 
		query.on("end", function () {
				 
		 //db_client.end();
	     return callback(undefined,ids_concat_titles);
			       
		}); 
		
}



exports.getAllStreamsOfType = function(type,language,callback){
	 

	 var query = db_client.query("SELECT s.*,sl.title,sl.description from STREAM s JOIN " +
	 		"STREAM2LANG sl ON sl.id = s.id WHERE s.type=$1 AND sl.language=$2",[type,language]);
	
	 query.on("row", function (row, result) {
	     result.addRow(row);
	    
	 });
	 
	 query.on("end", function (result) {
		 
	    // db_client.end();
	    
	     return callback(undefined,result.rows);
	       
	 }); 
	 
};
	 
// getCategoryStreams

exports.getTypeStreamsOfCategory = function(type,language,category,callback){
		 

	var query = db_client.query("SELECT s.*,sl.title,sl.description" +
	    " FROM STREAM s JOIN STREAM2CATEGORY sc ON sc.stream_id = s.id" +
			" JOIN CATEGORY c ON sc.category_id = c.id" +
			" JOIN STREAM2LANG sl ON sl.id = s.id" +
			" WHERE s.type=$1 AND sl.language=$2" +
			" AND c.id=$3", [type,language,category]);
	

	query.on("row", function (row, result) {
	    result.addRow(row);
		    
	});
		 
	query.on("end", function (result) {
			 
	 //db_client.end();
     return callback(undefined,result.rows);
		       
	}); 
	
};


exports.removeCategory = function(category_id,callback){
	
	// remove all labels from cache ** 

	console.log("the cat id " + category_id);
	 // remove from stream2category
	var query = db_client.query("DELETE from stream2category" +
			" WHERE category_id=$1" 
			, [category_id]);

	query.on("row", function (row, result) {
	    result.addRow(row);
		    
	});
		 
	query.on("end", function (result) {

	 // remove from category2lang

	  var query = db_client.query("DELETE from category2lang" +
			" WHERE id=$1" 
			, [category_id]);


	  	query.on("end", function (result) {

			 // remove from category

			  var query = db_client.query("DELETE from category" +
					" WHERE id=$1" 
					, [category_id]);

			  query.on("end", function (result) {

			
			 		return callback(undefined);

				});
			});
		});	
	}
	 

// getStreamByID

// LIVESTREAM  *** todo 

exports.getCategoryLabelsForStream= function(stream_id,current_language,callback){

var labels = []; 
var query = db_client.query("SELECT cl.label FROM stream2category sc" +
 " JOIN stream s on sc.stream_id = s.id JOIN category c on c.id=sc.category_id" +
 " JOIN category2lang cl on cl.id = sc.category_id" +
 " WHERE s.id=$1 AND cl.language=$2",[stream_id,current_language]);


query.on("row", function (row, result) {
	labels.push(row.label) 
	});

query.on("end", function (result) {
			 
	// db_client.end();
	console.log("results from getStreamCategories model " +
	 JSON.stringify(labels));
	return callback(labels);
		       
	});


}

exports.getStreamAds = function(stream_id,current_language,callback){
var query = db_client.query("SELECT s.id," +
			" ad.title as ad_title,ad.id as ad_id FROM STREAM s" +
			" JOIN stream2ad sa on sa.stream_id = s.id JOIN stream2lang ad on ad.id=sa.ad_id" +
			" WHERE s.id=$1 AND ad.language =$2",
	[stream_id,current_language]);

query.on("row", function (row, result) {
	    result.addRow(row);
		    
	});

query.on("end", function (result) {
			 
	// db_client.end();
		console.log("results from getStreamByID model " + JSON.stringify(result.rows));
     return callback(undefined,result.rows);
		       
	});


}

// get non language stream data 
// get category data based on current language *** 


// getStreamData *** 
exports.getStreamByID= function(stream_id,type,callback){

	var query;

	if (type == 'livestream'){
	 
	query = db_client.query("SELECT s.id,s.thumbnail,l.port," +
			" l.hostname,s.private,s.featured,l.application,l.rtmp_protocol,l.m3u8_protocol," +
			" to_char(s.pub_start, 'YYYY-MM-DD') as pub_start," +
			" to_char(s.pub_end, 'YYYY-MM-DD') as pub_end," +
			" s.private,l.streamname,l.video FROM STREAM s" +
			" JOIN LIVESTREAM l on l.id=s.id" +
			" WHERE s.id=$1", [stream_id]);

	}

	if (type == 'ondemand'){
	 
	query = db_client.query("SELECT s.id,s.thumbnail,o.video,"+
			" s.featured,s.private," +
			" to_char(s.pub_start, 'YYYY-MM-DD') as pub_start," +
			" to_char(s.pub_end, 'YYYY-MM-DD') as pub_end" +
			" FROM STREAM s" + 
			" JOIN ONDEMAND o on o.id=s.id" +
			" WHERE s.id=$1", [stream_id]);

	}

	if (type == 'advertisement'){
	 
	query = db_client.query("SELECT s.id,s.thumbnail,a.video" +
			" FROM STREAM s" +
			" JOIN advertisement a on a.id=s.id" +
			" WHERE s.id=$1", [stream_id]);

	}

	query.on("error",function(err){
		console.log("error in model " + err);
	});
	

	query.on("row", function (row, result) {
	    result.addRow(row);
		    
	});

		 
	query.on("end", function (result) {
			 
	// db_client.end();
		console.log("results from getStreamByID model " + JSON.stringify(result.rows));
     return callback(undefined,result.rows[0]);
		       
	});
};

// get all titles and descriptions in all enabled languages to show in edit stream form
exports.getAllStreamTranslations= function(stream_id,callback){

	var translated_titles = {};
  var translated_descriptions = {}; 
	 
	
	var query = db_client.query("SELECT * from stream2lang where id=$1", [stream_id]);
	
	
	query.on("row", function (row) {
			translated_titles[row.language] = row.title;
			translated_descriptions[row.language] = row.description; 
			
		});

	query.on("error", function (err) {
		console.log(err);
	    
	});
	 
	query.on("end", function () {
			 
	// db_client.end();
     return callback(undefined,translated_titles,translated_descriptions);
		       
	});
	
};

exports.tagCategoriesToAds


exports.clearCategories = function(stream_id,callback){

	var query = db_client.query("DELETE from stream2category WHERE stream_id=$1",[stream_id]);

		 
	query.on("end", function (result) {
		return callback(); 
	});

}

exports.clearTags = function(stream_id,callback){

	var query = db_client.query("DELETE from stream2tag WHERE stream_id=$1",[stream_id]);

		 
	query.on("end", function (result) {
		return callback(); 
	}); 

}


exports.clearAds = function(stream_id,callback){

	var query = db_client.query("DELETE from stream2ad WHERE stream_id=$1",[stream_id]);

		 
	query.on("end", function (result) {
		return callback(); 
	}); 

}



exports.removeStream= function(stream_id,type,callback){
	
	var query = db_client.query("DELETE from stream2lang where id=$1",[stream_id]);
	

	query.on("row", function (row, result) {
	    result.addRow(row);
		    
	});

		 
	query.on("end", function (result) {

		var query = db_client.query("DELETE from stream2category where stream_id=$1",[stream_id]);

		query.on("row",function(row,result) {
			result.addRow(row);
		});


		query.on("end",function(result){

			var query;


			if(type == 'advertisement'){

				query = db_client.query("DELETE from stream2ad where ad_id=$1",[stream_id]);

			}

			else{

				query = db_client.query("DELETE from stream2ad where stream_id=$1",[stream_id]);

			}

			query.on("row",function(row,result) {
				result.addRow(row);
			});
		


			query.on("end",function(result){


				var query_string = "DELETE from " + type + " where id=" + stream_id;

				var query = db_client.query(query_string); 


				query.on("row",function(row,result){
					result.addRow(row);
				});

				query.on("error", function (err) {
		    console.log("AN ERROR " + err)
			    
				});

				query.on("end",function(result){

							var query = db_client.query("DELETE from stream2tag where stream_id=$1"
					, [stream_id]);



					query.on("row",function(row,result){
						result.addRow(row);
					});

					query.on("error", function (err) {
			    console.log("AN ERROR " + err)
				    
					});

					query.on("end",function(result){

								var query = db_client.query("DELETE from user2watched where"+
								" stream_id=$1", [stream_id]);


									query.on("end",function(result){
												var query = db_client.query("DELETE from user2favourites "+
													"where stream_id=$1", [stream_id]);


									query.on("end",function(result){

										var query = db_client.query("DELETE from votes "+
												"where stream_id=$1", [stream_id]);


										query.on("end",function(result){

											var query = db_client.query("DELETE from stream where id=$1"
										, [stream_id]);

												query.on("end",function(result){


													var query = db_client.query("DELETE from votes where stream_id=$1"
												, [stream_id]);

												query.on("end",function(result){

										// db_client.end();
									     return callback(undefined,result.rows);
									   	});
										})
									})
								});
							}); 
						});
					});
				});
			});
		});
	}


	
// POSTS 
// category is a map of the category labels translations 
// {en:news,fr:nouvelles} 
// return err,new category id,translation labels 
exports.addCategory= function(category,stream_type,langs,callback){

	var new_catID; 
	

	// add new category to database 
	var query = db_client.query(
			"INSERT INTO category (type) " +
			"VALUES ($1) " + 
			"RETURNING id", [stream_type]);

		// CATCH DUPLICATE TITLE VIOLATIONS ***** 
	
		query.on("row", function (row, result) {
	    result.addRow(row);
		    
		});

		query.on("error",function(err){
			console.log(err);
			return callback(category); 
		});
		 
		query.on("end", function (insert_result) {

			new_catID = insert_result.rows[0].id;
			console.log("the category "  + JSON.stringify(category)); 
			

			const insert_values = langs.map(lang => `(${new_catID},'${lang}','${category["" + lang]}')`).join(', ');
		  console.log("the insert values " + insert_values); 
			
			var query_string="INSERT into category2lang values " + insert_values;

			console.log("the query string " + query_string);
		
			var query = db_client.query(query_string);

		
			// for each label translation field in form 


			query.on("error",function(err){
				console.log(err);
			
				return callback(err,undefined,undefined); 
			});

			// insert translation data 
			query.on("end", function () {
				console.log("ended");


				return callback(undefined,new_catID,category);
			});
		});
	}


exports.addLiveStream= function(req,ad_ids,callback){
	
	var sess = req.session; 
	//var title_en = req.body.title-en;
	//var description_en =  req.body.description;
	//var m3u8_protocol = req.body.protocol;
	var hostname = req.body.hostname;
	var port = parseInt(req.body.port);
	var application = req.body.application;
	var rtmp_protocol = req.body['rtmp-protocol'];
	var m3u8_protocol = req.body['m3u8-protocol'];
	var pub_start = req.body['pub-start'] == "" ? null : req.body['pub-start'];
	// defaults to infinity in db
	var pub_end = req.body['pub-end']== "" ? 'infinity' : req.body['pub-end'];
	var featured =  req.body['featured'] == 1 ? true : false;
	var private =  req.body['private'] == 1 ? true : false; 
	var streamname = req.body['streamname'] 

	//var streamname = req.body.streamname;
	var thumb_path = req.body.thumb;
	
	
	var current_language= sess.current_language; 

	var streamID; 
	

	// for each label translation field in form 
	getTranslations(req,function(title_trans,description_trans){

		// multiple inserts into stream2lang table
		// for every entry in title_trans and description_trans

		// construct URL 

		var url = constructLivestreamURL(m3u8_protocol,hostname,port,application,
			streamname);

		// insert numerical data 
		var query = db_client.query("WITH ins AS (" +
			"INSERT INTO stream " +
			"(type,thumbnail,pub_start,pub_end,featured,private) VALUES ($1,$2,$8,$9,$10,$11 ) " + 
			"RETURNING id) " +
			"INSERT INTO livestream " +
			"(id,port,hostname,application,rtmp_protocol,m3u8_protocol,video,streamname) " +
			"SELECT id, $3,$4,$5,$6,$7,$12,$13 FROM ins RETURNING id"
		, ['livestream',thumb_path,port,hostname,application,rtmp_protocol,m3u8_protocol,
		pub_start,pub_end,featured,private,url,streamname]
	   );

		// CATCH DUPLICATE TITLE VIOLATIONS ***** 
	
		query.on("row", function (row, result) {
	    result.addRow(row);
		    
		});
		 
		query.on("end", function (insert_result) {
		
			// insert translation data 

		
			// add new livestream to its categories 
			var langs = req.app.locals.enabled_languages; 

			streamID = insert_result.rows[0].id;
				

			const insert_values = langs.map(lang => `(${streamID},'${lang}','${title_trans[lang]}','${description_trans[lang]}')`).join(', ');
			
			
			var query_string="INSERT into stream2lang values " + insert_values;
			console.log("the query string " + query_string);
			var query = db_client.query(query_string);


			// tag this stream to its categories 
			query.on("end", function (result) {


				// tag this stream to its ads 

				console.log("the ad ids " + ad_ids); 

				tagStreamToAds(streamID,ad_ids, function(){
				// no categories to tag to 
				return callback(undefined,streamID);
				});
				
			});

		});

	});

};

exports.editVOD = function(id,req,callback){
	var sess = req.session; 

	var pub_start = req.body['pub-start'] == "" ? null : req.body['pub-start'];
	var pub_end = req.body['pub-end']== "" ? 'infinity' : req.body['pub-end'];
	var featured =  req.body['featured'] == 1 ? true : false;
	var private =  req.body['private'] == 1 ? true : false; 

	console.log("the pub_end in model " + pub_end);
	//var streamname = req.body.streamname;
	var thumb_path = req.body['thumb'];
	var video_path = req.body['video']; 

	
	var current_language= sess.current_language; 

	
	// for each label translation field in form 
	getTranslations(req,function(title_trans,description_trans){

		// multiple inserts into stream2lang table
		// for every entry in title_trans and description_trans


		// insert numerical data 
		var query = db_client.query("UPDATE stream" +
			" SET thumbnail=$1,pub_start=$2,pub_end=$3,featured=$4,private=$5" +
			" WHERE id=$6",[thumb_path,pub_start,pub_end,featured,private,id]); 
		
		query.on("end", function (insert_result) {

			var query = db_client.query("UPDATE ondemand SET " +
			" video=$1 WHERE id=$2", [video_path,id]);
		
			// insert translation data 

			query.on("end", function (insert_result) {

				// add new livestream to its categories 
				var langs = req.app.locals.enabled_languages; 

				// clear old titles and descriptions
				var query = db_client.query("DELETE from stream2lang WHERE id=$1"
			  ,[id]); 

			  query.on("end", function (insert_result) {
		
					const insert_values = langs.map(lang => 
					`(${id},'${lang}','${title_trans[lang]}','${description_trans[lang]}')`)
					.join(', ');
					
					var query_string="INSERT into stream2lang values " + insert_values;
					console.log("the query string " + query_string);
					var query = db_client.query(query_string);

					// tag this stream to its categories 
					query.on("end", function (result) {
						
						return callback(undefined);
						
					});

					// stream DNE 
					query.on("error",function(err){
						return callback(err);
					})
				});
			});
		});
	});
};


exports.editLiveStream = function(id,req,callback){

	var sess = req.session; 

	var hostname = req.body.hostname;
	var port = parseInt(req.body.port);
	var application = req.body.application;
	var rtmp_protocol = req.body['rtmp-protocol'];
	var m3u8_protocol = req.body['m3u8-protocol'];
	var pub_start = req.body['pub-start'] == "" ? null : req.body['pub-start'];
	var pub_end = req.body['pub-end']== "" ? null : req.body['pub-end'];
	var featured =  req.body['featured'] == 1 ? true : false;
	var private =  req.body['private'] == 1 ? true : false; 
	var streamname = req.body['streamname'] 

	//var streamname = req.body.streamname;
	var thumb_path = req.body.thumb;

	
	var current_language= sess.current_language; 

	// for each label translation field in form 
	getTranslations(req,function(title_trans,description_trans){

		// multiple inserts into stream2lang table
		// for every entry in title_trans and description_trans

		// construct URL 

		var url = constructLivestreamURL(m3u8_protocol,hostname,port,application,
			streamname);

		// insert numerical data 
		var query = db_client.query("UPDATE stream" +
			" SET thumbnail=$1,pub_start=$2,pub_end=$3,featured=$4,private=$5" +
			" WHERE id=$6",[thumb_path,pub_start,pub_end,featured,private,id]); 
		
		query.on("end", function (insert_result) {

			var query = db_client.query("UPDATE livestream SET " +
			" port=$1,hostname=$2,application=$3,rtmp_protocol=$4,m3u8_protocol=$5,video=$6" +
			",streamname=$7 WHERE id=$8", [port,hostname,application,rtmp_protocol,
		 m3u8_protocol,url,streamname,id]);
		
			// insert translation data 

			query.on("end", function (insert_result) {

				// add new livestream to its categories 
				var langs = req.app.locals.enabled_languages; 

				// clear old titles and descriptions
				var query = db_client.query("DELETE from stream2lang WHERE id=$1"
			  ,[id]); 

			  query.on("end", function (insert_result) {
		
					const insert_values = langs.map(lang => 
					`(${id},'${lang}','${title_trans[lang]}','${description_trans[lang]}')`)
					.join(', ');
					
					
					var query_string="INSERT into stream2lang values " + insert_values;
					console.log("the query string " + query_string);
					var query = db_client.query(query_string);


					// tag this stream to its categories 
					query.on("end", function (result) {
						
						return callback(undefined);
						
					});
				});
			});
		});
	});
};

exports.editAdvertisement = function(id,req,callback){

	var sess = req.session;
	var pub_start = req.body['pub-start'] == "" ? null : req.body['pub-start'];
	var pub_end = req.body['pub-end']== "" ? null : req.body['pub-end'];
	var featured =  req.body['featured'] == 1 ? true : false;
	var private =  req.body['private'] == 1 ? true : false; 

	//var streamname = req.body.streamname;
	var thumb_path = req.body['thumb'];

	var video_path = req.body['video']; 

	
	var current_language= sess.current_language; 

	
	// for each label translation field in form 
	getTranslations(req,function(title_trans,description_trans){

		// multiple inserts into stream2lang table
		// for every entry in title_trans and description_trans

		// insert numerical data 
		var query = db_client.query("UPDATE stream" +
			" SET thumbnail=$1,pub_start=$2,pub_end=$3,featured=$4,private=$5" +
			" WHERE id=$6",[thumb_path,pub_start,pub_end,featured,private,id]); 
		
		query.on("end", function (insert_result) {

			var query = db_client.query("UPDATE advertisement SET " +
			" video=$1 WHERE id=$2", [video_path,id]);
		
			// insert translation data 

			query.on("end", function (insert_result) {

				// add new livestream to its categories 
				var langs = req.app.locals.enabled_languages; 

				// clear old titles and descriptions
				var query = db_client.query("DELETE from stream2lang WHERE id=$1"
			  ,[id]); 

			  query.on("end", function (insert_result) {
		
					const insert_values = langs.map(lang => 
					`(${id},'${lang}','${title_trans[lang]}','${description_trans[lang]}')`)
					.join(', ');
					
					
					var query_string="INSERT into stream2lang values " + insert_values;
					console.log("the query string " + query_string);
					var query = db_client.query(query_string);


					// tag this stream to its categories 
					query.on("end", function (result) {
						
						return callback(undefined);
						
					});
				});
			});
		});
	});
};

exports.getTagsForStream = function(stream_id,callback){

	var tags = []; 
	var query = db_client.query("SELECT label FROM tag t JOIN stream2tag "
		+ " st on st.tag_id=t.id WHERE st.stream_id=$1",
		[stream_id]);

	query.on("row", function (row, result) {
	   tags.push(row.label);
	});
		 
	query.on("end", function (result) {

		callback(tags); 
		});

}

exports.tagStreamToTags = function(tags,stream_id,callback){
	
	var done = 0; 
	
 if (tags.length > 0 && !tags[0]==""){

	// get id for the tags
	for (var i = 0 ; i < tags.length ; i ++){
		
		var tag = tags[i]; 

		getTagIDByLabel(tag,function(exists,tag_id,tag_label){
			// tag dne , create
			if(!exists){
				console.log("the tag " + tag_label); 
				createTag(tag_label,function(tag_id){
					console.log("in createtag the tag id " + tag_id); 
					tagToStream(tag_id,stream_id,function(err){
						console.log("intagtostream");
						done ++; 

						if(done == tags.length){

						callback(undefined)
						}
					}); 
				})
			
			}
			else{
			   tagToStream(tag_id,stream_id,function(){
			   		done ++; 


					if(done == tags.length){

						callback(undefined)
						}
				})
			 }

		})

	}

	if(done == tags.length){

						callback(undefined)
						}

					}
// no tags to add
else{ 
			callback(undefined);
	}
}

exports.addVOD= function(req,ad_ids,callback){
		
	var sess =req.session;
	var thumb_path = req.body.thumb;
	var video_path = req.body.video;
	

	console.log("the req.body " + JSON.stringify(req.body));
	var pub_start = req.body['pub-start'] == "" ? null : req.body['pub-start'];
	// defaults to infinity in db
	var pub_end = req.body['pub-end']== "" ? null : req.body['pub-end'];
	var featured =  req.body['featured'] == 1 ? true : false;
	var private =  req.body['private'] == 1 ? true : false; 

	var current_language = sess.current_language; 


	var streamID; 

	// for each label translation field in form 
	getTranslations(req,function(title_trans,description_trans){

		// multiple inserts into stream2lang table
		// for every entry in title_trans and description_trans

		// insert numerical data 
		var query =  db_client.query("WITH ins AS (" +
			"INSERT INTO stream " +
			"(type,thumbnail,pub_start,pub_end,featured,private) VALUES ($1,$2,$4,$5,$6,$7) " + 
			"RETURNING id) " +
			"INSERT INTO ondemand " +
			"(id,video) " +
			"SELECT id,$3 FROM ins RETURNING id"
		, ['ondemand',thumb_path,video_path,pub_start,pub_end,featured,private]
	   );
		// CATCH DUPLICATE TITLE VIOLATIONS ***** 
	
		query.on("row", function (row, result) {
	    result.addRow(row);
		    
		});
		 
		query.on("end", function (insert_result) {
		
			// insert translation data 

			// add new livestream to its categories 
			var langs = req.app.locals.enabled_languages; 

			streamID = insert_result.rows[0].id;
			
			const insert_values = langs.map(lang => `(${streamID},'${lang}',
				'${title_trans[lang]}','${description_trans[lang]}')`).join(', ');
			
			
			var query_string="INSERT into stream2lang values " + insert_values;

			console.log("the query string " + query_string);
			var query = db_client.query(query_string);


			// tag this stream to its categories 
			query.on("end", function (result) {


				// tag this stream to its ads 

					console.log("the ad ids " + ad_ids); 

				tagStreamToAds(streamID,ad_ids, function(){
					
				 return callback(undefined,streamID);

				});
			});
		});
	});
};


exports.tagStreamToAds = function(stream_id, ad_ids,callback){

	var valid_ad_ids=[];

	if (ad_ids.length > 0 && !ad_ids[0]==""){

		// get the category ids in category ids that exist in db
		var query_string = "SELECT * from advertisement WHERE id in (" + ad_ids
			+ ")"

		console.log("The query string in tagStreamToAds " + query_string)

		var exists_query = db_client.query(query_string);

		exists_query.on("row",function(row,result){
			valid_ad_ids.push(row.id); 

		})

		exists_query.on("end",function(result){
			const insert_values2 = valid_ad_ids.map(adID => `(${stream_id}, ${adID})`).join(', ');
				
			var query_string="INSERT into stream2ad values " + insert_values2;
		
			var query = db_client.query(query_string);

			query.on("error", function(err){
				console.log("the error in model " + err); 
				callback(err); 
			})

 			query.on("end",function(result){
				return callback(undefined);
			});

		 query.on("error", function (err) {
			// stream DNE
	  	return callback(err);	
		});    

		});
	}

 // no  ads to add	
 else{
 	 return callback(undefined); 
 }

}


// only tags streams to categories that exist
exports.tagStreamToCategories = function(streamID,category_ids,callback){

var valid_category_ids = []; 


if (category_ids.length > 0 && !category_ids[0]==""){

	// get the category ids in category ids that exist in db
	var query_string = "SELECT * from category WHERE id in (" + category_ids
		+ ")"

	console.log("The query string in tagStreamToCategories " + query_string)

	var exists_query = db_client.query(query_string);

	exists_query.on("row",function(row,result){
		valid_category_ids.push(row.id); 

	})

	// tag stream to the valid categories
	exists_query.on("end",function(result){

		const insert_values = valid_category_ids.map(categoryId =>
	 `(${streamID}, ${categoryId})`).join(', ');
	
		var query_string="INSERT into stream2category values " + insert_values;
		console.log("the query string " + query_string);
		var query = db_client.query(query_string);
	
		query.on("end", function (result) {

			// inform request of categories that have not been added **TODO
			// if some categories in categories_id were not valid
			
	  	return callback(undefined);	
		});   

			query.on("error", function (err) {
			// stream DNE
	  	return callback(err);	
		});    

		 

		
	})
}
	
// categories empty
else{

		return callback(undefined); 
	}
}

exports.addAdvertisement= function(req,callback){
	
	var sess = req.session; 
	var thumb_path = req.body.thumb;
	var video_path = req.body.video;
	
	var streamID; 
	
	var current_language = sess.current_language; 


	

	// for each label translation field in form 
	getTranslations(req,function(title_trans,description_trans){

		// multiple inserts into stream2lang table
		// for every entry in title_trans and description_trans

		// insert numerical data 


		var query = db_client.query("WITH ins AS (" +
			"INSERT INTO stream " +
			"(type,thumbnail) VALUES ($1,$2) " + 
			"RETURNING id) " +
			"INSERT INTO advertisement " +
			"(id,video) " +
			"SELECT id,$3 FROM ins RETURNING id"
		, ['advertisement',thumb_path,video_path]
	   );
		// CATCH DUPLICATE TITLE VIOLATIONS ***** 
	
		query.on("row", function (row, result) {
	    result.addRow(row);
		    
		});
		 
		query.on("end", function (insert_result) {
		
			// insert translation data 

		
			// add new livestream to its categories 
			var langs = req.app.locals.enabled_languages; 


			streamID = insert_result.rows[0].id;
			
	
			const insert_values = langs.map(lang => `(${streamID},'${lang}','${title_trans[lang]}','${description_trans[lang]}')`).join(', ');
			
			
			var query_string="INSERT into stream2lang values " + insert_values;
			console.log("the query string " + query_string);
			var query = db_client.query(query_string);


			// tag this stream to its categories 
			query.on("end", function (result) {
				
					
			 return callback(undefined,streamID);

		});

	});

});
};


exports.editCategory = function(category_id,new_label,
	language,type,callback){

	var query = db_client.query("UPDATE category2lang cl set label=$1" +
	" WHERE id=$2 AND language=$3",
	[new_label,category_id,language]); 


	query.on("row", function (row, result) {
    result.addRow(row);
	    
	});

	query.on("error", function (err) {
    console.log(err)
	    
	});
	 
	query.on("end", function (result) {
		// update cache 
		callback(undefined); 
	})
}




// ********HELPERS***********

// get translation fields (title, description) from add new stream form
function getTranslations(req,callback){
	var title_trans = new Object();
	var description_trans = new Object(); 
	var langs = req.app.locals.enabled_languages; 
	for (i = 0; i < langs.length; i++) { 
   

  	var title_lang_tag = 'title-' + langs[i];
  	var desc_lang_tag = 'description-' + langs[i];
    console.log("the title tag : " + title_lang_tag);
    var translated_title = req.body[title_lang_tag]; 
    var translated_description = req.body[desc_lang_tag]
    title_trans[langs[i]]=translated_title;
    description_trans[langs[i]]=translated_description;
    // db loop :
    // check cache if label already exists  ** TODO** 

    // assign req.body[translation] value to category label for lang=[translation]
  	}
  if(i==langs.length){
  	if(typeof callback =='function'){
  		callback(title_trans,description_trans);
  	}
  	

  }
}

function getObjIDs(objs,callback){
	var obj_ids = []
	for(var i = 0; i < objs.rows.length; i++) {
	    var obj = objs.rows[i];

	    obj_ids.push(obj.id)
	}
	if (i==objs.rows.length){
		if(typeof callback=='function'){
			callback(obj_ids);
		}
	}
}

// CHACHE FUNCT ** 


function getTagIDByLabel(tag,callback){
  // select id from t.tag from tag t join tag2category tc on tc.id=t.id
  // where tc.label=$1 and tc.language=$2, [tag,language]

  console.log("the tag " + tag);
  var query = db_client.query('SELECT id FROM tag' +
  	 ' WHERE label=$1',[tag]);

  query.on("row", function (row, result) {
	    result.addRow(row);
		    
	});

		query.on("error", function (err) {
	    console.log("errr in getTAgIDby label " + err)
		    
	});
		 
	query.on("end", function (result) {
			// if tag DNE 
			if(result.rows.length == 0){
				// false for tag DNE 
				callback(false,undefined,tag); 
			}

			// tag exists
			else{
				callback(true,result.rows[0].id,tag)
			}

		})
			
 

  // on end 
  // if result is null
  	// callback(false,undefined) 	
	//callback(true,id) 

}; 


function createTag(label,callback){

 console.log("in createtag the label " + label)


	// add new tag to database 
	var query = db_client.query(
			"INSERT INTO tag (label) " +
			"VALUES ($1) " + 
			"RETURNING id",[label]);

		// CATCH DUPLICATE TITLE VIOLATIONS ***** 
	
		query.on("row", function (row, result) {
	    result.addRow(row);
		    
		});

		query.on("error",function(err){
			console.log(err);
			callback(err); 
		});
		 
		query.on("end", function (result) {

			new_tagID = result.rows[0].id;
			console.log("the new tag id " + new_tagID);

			callback(new_tagID);

			// insert tag to all translations 

	// callback(id) 
});
	}


function constructLivestreamURL(m3u8_protocol,hostname,port,application,
			streamname){

  var tokenized = wowza.urlGenerate({
    domain: hostname + ":" + port, // hostname : port 
    urlPrefix: application + "/smil:" + streamname + ".smil", // application name/smil: + streamname .smil/playlist.m3u8
    sharedSecret: process.env.WOWZA_SECRET,
    prefixParameter: process.env.WOWZA_SECRET, //default value is wowzatoken 
    startTime: moment(), //new Date() // starttime 
    endTime: moment().add(4,'hours'),
	});

	// configure to protocol 
	tokenized['https'] = tokenized['http'].replace("http","https"); 

	return tokenized.https; 

}

function tagToStream(tag_id,stream_id,callback){
	// add new tag to database 
	console.log("tagging");
	var query = db_client.query("INSERT INTO stream2tag (stream_id,tag_id)"
		+ " VALUES ($1,$2)" , [stream_id,tag_id]);

		// CATCH DUPLICATE TITLE VIOLATIONS ***** 
	
		query.on("row", function (row, result) {
	    result.addRow(row);
		    
		});

		query.on("error",function(err){
			// stream dne 
			return callback(err); 
		});
		 
		query.on("end", function (result) {

		
				
			callback(undefined);

			// insert tag to all translations 

	// callback(id) 
});
	}



	// callback

// tag stream only to ads that exist in db
function tagStreamToAds(streamID,ad_ids,callback){
	console.log(" ad ids lenght " + ad_ids.length)

	var valid_ad_ids = [];

	if(ad_ids.length > 0 && ad_ids != null){

		// get the category ids in category ids that exist in db
		var query_string = "SELECT * from advertisement WHERE id in (" + ad_ids
			+ ")"

		console.log("The query string in tagStreamToAds " + query_string)

		var exists_query = db_client.query(query_string);

		exists_query.on("row",function(row,result){
			valid_ad_ids.push(row.id); 

		})

		exists_query.on("end",function(result){
			const insert_values2 = valid_ad_ids.map(adID => `(${streamID}, ${adID})`).join(', ');
				
						
			var query_string="INSERT into stream2ad values " + insert_values2;
		
			var query = db_client.query(query_string);

			query.on("row", function (row, result) {
				result.addRow(row);

			});

			query.on("error", function(err){
				console.log("the error in model " + err); 
				return callback()
			})

	 		query.on("end",function(result){
	 			if(typeof callback=='function'){
					callback();
				}
			});
		})
	}

else{

		if(typeof callback=='function'){
			callback();


		}
	}
}




	
	
