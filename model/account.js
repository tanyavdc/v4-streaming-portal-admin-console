// load all the things we need
	var LocalStrategy   = require('passport-local').Strategy;
	
	var db = require('./database.js');
	
	var sess; 
	
	    // expose this function to our app using module.exports
	module.exports = function(passport) {
	
	    // =========================================================================
	// passport session setup ==================================================
	// =========================================================================
	// required for persistent login sessions
	// passport needs ability to serialize and unserialize users out of session
	
	// used to serialize the user for the session
	passport.serializeUser(function(user, done) {
		var user_data = {id:user.id,email:user.email}
	
	  done(null, user_data);
	});
	
	// used to deserialize the user
	passport.deserializeUser(function(user, done) {
		
		done(null,user);
	    });

  // admin login 
	passport.use('local-login', new LocalStrategy({
	        // by default, local strategy uses username and password, we will override with email
	        usernameField : 'email',
	        passwordField : 'password',
	       passReqToCallback : true // allows us to pass back the entire request to the callback
	    },function(req,email, password, done) {
	    	var sess = req.session;
		
	    	
	    	var query = db.query("SELECT * from users WHERE" +
	    	" email=$1 AND type='admin'", [email]);
	    	
	    	query.on("row", function (row, result) {
	    		
	    	    result.addRow(row);
	    		    
	    	});


	    	query.on("end", function (result) {   	
	    			 
	    		var rows = result.rows;
	    		
	    		 if (!rows.length) {
		 						
               return done(null, false, 
               	req.flash('loginMessage', 'No User Found')
               	); 
               // req.flash is the way to set flashdata using connect-flash
           } 

			
						// if the user is found but the password is wrong
					 var db_password_hash = rows[0].password; 
           var passwords_match = bcrypt.compareSync(password,db_password_hash); 
           
           if (!passwords_match){
          
               return done(null, false,
               	req.flash('loginMessage', 'Wrong password.')); 
               // create the loginMessage and save it to session as flashdata
           }
           // all is well, return succes	sful user
        	 sess.user.logged_in = true;
           return done(null, rows[0]);			    		       
  				}); 	
	    	}
	    ));
		};
	    
	    
	   

