/**
 * REDIS CACHE CONN 
 */

var redis = require("redis");
require('redis-delete-wildcard')(redis);

var client = redis.createClient();

module.exports = client; 